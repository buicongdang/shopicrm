let mix = require('laravel-mix');

const CleanWebpackPlugin = require('clean-webpack-plugin');

// paths to clean
var pathsToClean = [
    'public/bower_components',
    'public/dist',
    'public/images'
];

// the clean options to use
var cleanOptions = {};

mix.webpackConfig({
    plugins: [
        new CleanWebpackPlugin(pathsToClean, cleanOptions)
    ]
});
/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

// mix.js('resources/assets/js/app.js', 'public/js')
//    .sass('resources/assets/sass/app.scss', 'public/css');

mix.js('resources/assets/js/bootstrap.js', 'public/dist/js');

mix.less('resources/assets/build/less/AdminLTE-without-plugins.less',
    'public/dist/css/adminlte.min.css'
).version();

mix.less('resources/assets/build/less/skins/skin-blue.less',
    'public/dist/css/skin-blue.min.css'
).version();

mix.styles(
    'resources/assets/dist/css/page/campaigns_auto.css',
    'public/dist/css/campaigns_auto.min.css'
).version();

mix.scripts(
    'resources/assets/dist/js/adminlte.js',
    'public/dist/js/adminlte.min.js'
).version();

mix.scripts(
    'resources/assets/dist/js/pages/membership.js',
    'public/dist/js/pages/membership.min.js'
).version();
//
// mix.js(
//     'resources/assets/js/broadcasting.js',
//     'public/dist/js/broadcasting.js'
// );

mix.scripts(
    'resources/assets/dist/js/pages/campaigns_auto.js',
    'public/dist/js/pages/campaigns_auto.min.js'
).version();

mix.copyDirectory('resources/assets/bower_components', 'public/bower_components');

mix.copyDirectory('resources/assets/dist/img', 'public/images');
