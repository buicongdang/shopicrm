<?php
return [
    'general' => [
        'name' => 'General',
        'slug' => 'general',
        'meta' => false,
        'list' => [
            [
                'slug' => 'first_buyer',
                'name' => 'First Buyer',
                'background' => ''
            ],
            [
                'slug' => 'birthday',
                'name' => 'Customer Birthday',
                'background' => ''
            ]
        ]
    ],
    'member_ship' => [
        'name' => 'Member Ship',
        'slug' => 'member_ship',
        'meta' => false,
        'list' => 'maps.member_ship',
    ],
    'before_buy' => [
        'name' => 'Before Buy',
        'slug' => 'before_buy',
        'meta' => [
            'name' => 'before_time',

        ]
    ]
];