<?php
return [
   'scope' => [
       'read_products',
       'write_products',
       'read_customers',
       'write_customers',
       'read_orders',
       'write_orders',
       'read_draft_orders',
       'write_draft_orders',
       'read_price_rules',
       'write_price_rules'
   ],
   'redirect_before_install' => env('APP_DOMAIN').'/authApp'
];