<?php
return [
    'pagination' => 20,
    'status' => [
        'unpublish' => 0,
        'publish' => 1
    ],
	'api_limit' => 100
];