<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CampaignsAuto extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('campaigns_auto', function (Blueprint $table) {
            $table->increments('id');
            $table->string('shop_id');
            $table->integer('id_member');
            $table->string('type');
            $table->string('title');
            $table->string('email_from');
            $table->string('name_from');
            $table->string('email_template');
            $table->string('meta');
            $table->timestamps();

            $table->index('id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('campaigns_auto');
    }
}
