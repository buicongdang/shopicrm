<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Orders extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->string('id');
	        $table->string('shop_id');
	        $table->string('email')->nullable();
            $table->string('total_price')->nullable();
            $table->string('currency')->nullable();
            $table->string('fulfillment_status')->nullable();
            $table->string('financial_status')->nullable();
            $table->string('products')->nullable();
            $table->string('customer')->nullable();
            $table->timestamps();

            $table->primary('id');
            $table->index('id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
