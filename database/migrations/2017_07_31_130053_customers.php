<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Customers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('customers', function(Blueprint $table) {
            $table->increments('id');
            $table->string('id_customer')->nullable();
            $table->string('shop_id');
            $table->string('first_name')->nullable();
            $table->string('last_name')->nullable();
            $table->string('email_primary');
            $table->string('emails')->nullable();
            $table->string('phone_primary')->nullable();
            $table->string('phones')->nullable();
            $table->dateTime('birth_day')->nullable();
            $table->string('avatar')->nullable();
            $table->integer('member_level')->nullable();
            $table->integer('status')->default(config('common.status.publish'));
            $table->string('address')->nullable();
            $table->string('province')->nullable();
            $table->string('city')->nullable();
            $table->string('country')->nullable();
            $table->string('country_code')->nullable();
            $table->boolean('accepts_marketing')->default(1);
            $table->timestamps();

            $table->index('id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('customers');
    }
}
