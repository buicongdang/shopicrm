<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Charged extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('charged', function (Blueprint $table) {
            $table->increments('id');
            $table->string('billing_id');
            $table->string('billing_status');
            $table->string('billing_name');
            $table->string('billing_cost');
            $table->string('activated_on');
            $table->string('trial_ends_on');
            $table->string('cancelled_on');
            $table->string('trial_days');
            $table->timestamps();

            $table->index('id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('charged');
    }
}
