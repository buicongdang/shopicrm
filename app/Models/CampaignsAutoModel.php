<?php

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class CampaignsAutoModel extends Model
{
    protected $table = 'campaigns_auto';

    protected $fillable = [
        'shop_id',
        'type',
        'title',
        'email_from',
        'name_from',
        'email_template',
        'meta',
        'created_at',
        'updated_at',
    ];

    /**
     * @var bool
     */
    public $timestamps = true;
}