<?php
/**
 * Created by PhpStorm.
 * User: snakexxx
 * Date: 02/08/2017
 * Time: 14:53
 */

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class ShopsModel extends Model
{
    /**
     * @var string
     */
    protected $table = 'shops';

    /**
     * @var array
     */
    protected $fillable = [
        'id',
        'name',
        'domain',
        'email',
        'phone',
        'shop_owner',
        'plan_name',
        'myshopify_domain',
        'shop_created_at',
        'province',
        'country',
        'currency',
        'iana_timezone'
    ];

    /**
     * @var bool
     */
    public $timestamps = true;
}