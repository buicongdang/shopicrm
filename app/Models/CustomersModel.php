<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

/**
 * Class CustomersModel
 * @package App\Models
 */
class CustomersModel extends Model
{

    protected $primaryKey = 'id';
    /**
     * @var string
     */
    protected $table = 'customers';

    /**
     * @var array
     */
    protected $fillable = [
        'shop_id',
        'id_customer',
        'first_name',
        'last_name',
        'email_primary',
        'emails',
        'phone_primary',
        'phones',
        'birth_day',
        'avatar',
        'member_level',
        'status',
        'address',
        'province',
        'city',
        'country',
        'country_code',
        'accepts_marketing'
    ];

    /**
     * @var bool
     */
    public $timestamps = true;
}