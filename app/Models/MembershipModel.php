<?php
/**
 * Created by PhpStorm.
 * User: buicongdang
 * Date: 7/31/17
 * Time: 8:51 PM
 */

namespace App\Models;


use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;


class MembershipModel extends Model{
	/**
	 * @var string
	 */
	protected $table = 'membership';

	protected $fillable = [
		'name',
		'description',
		'image',
		'order_price',
		'created_at',
		'updated_at',
	];

	/**
	 * @var bool
	 */
	public $timestamps = true;
}