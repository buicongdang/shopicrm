<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class OrdersModel
 * @package App\Models
 */
class OrdersModel extends Model
{
    /**
     * @var string
     */
    protected $table = 'orders';
    /**
     * @var array
     */
    protected $fillable = [

    ];
    public $timestamps = true;
}
