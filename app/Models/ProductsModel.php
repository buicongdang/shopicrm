<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class ProductsModel
 * @package App\Models
 */
class ProductsModel extends Model
{
    /**
     * @var string
     */
    protected $table = 'products';
    /**
     * @var array
     */
    protected $fillable = [
        'id',
        'shop_id',
        'title',
        'images',
        'body_html',
        'handle'
    ];
    public $timestamps = true;
}
