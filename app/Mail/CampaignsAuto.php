<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class CampaignsAuto extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $contentMail = file_get_contents('test.blade.php');
        return $this->subject('Hello world!!!')->from('campaigns@vistapp.co')->view('mail.hello', compact('contentMail'));
//        return $this->view('mail.hello');
    }
}
