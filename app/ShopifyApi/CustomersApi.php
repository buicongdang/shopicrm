<?php

namespace App\ShopifyApi;


use App\Services\ShopifyServices;
use Contract\ShopifyAPI\CustomersApiInterface;

class CustomersApi extends ShopifyServices implements CustomersApiInterface
{
	/**
	 * CustomersApi constructor.
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * @param array $field
	 * @param int $page
	 * @param int $limit
	 * @return array
	 */
	public function all(array $field = [], int $page = 1, int $limit = 250) : array
	{
		try{
			$field = implode(',', $field);
			$customers = $this->_shopify->call(
				[
					'URL' => 'customers.json',
					'METHOD' => 'GET',
					'DATA' => [
						'limit' => $limit,
						'page' => $page,
						'fields' => $field,
					]
				]
			);
			return ['status' => true, 'customers' => $customers->customers];
		} catch (\Exception $exception)
		{
			return ['status' => false, 'message' => $exception->getMessage()];
		}
	}

	/**
	 * @param array $field
	 * @param string $customer
	 * @return array
	 */
	public function detail(array $field = [], string $customer) : array
	{
		try{
			$field = implode(',', $field);
			$customer = $this->_shopify->call(
				[
					'URL' => 'customers/'.$customer.'.json',
					'METHOD' => 'GET',
					'DATA' => [
						'fields' => $field
					]
				]
			);
			return ['status' => true, 'customer' => $customer->customer];
		} catch (\Exception $exception)
		{
			return ['status' => false, 'message' => $exception->getMessage()];
		}
	}

	/**
	 * @return array
	 */
	public function count() : array
	{
		try{
			$count = $this->_shopify->call(
				[
					'URL' => '/admin/customers/count.json',
					'METHOD' => 'GET',
				]
			);
			return ['status' => true, 'count' => $count->count];

		} catch (\Exception $exception)
		{
			return ['status' => false, 'message' => $exception->getMessage()];
		}
	}

	/**
	 * @param array $data
	 * @return array
	 */
	public function create(array $data) : array
	{
		try{
			$customer = $this->_shopify->call(
				[
					'URL' => '/admin/customers.json',
					'METHOD' => 'POST',
					'DATA' => [
						'customer' => $data
					]
				]
			);
			return ['status' => true, 'customer' => $customer->customer];
		} catch (\Exception $exception)
		{
			return ['status' => false, 'message' => $exception->getMessage()];
		}
	}

	/**
	 * @param string $customer
	 * @param array $data
	 * @return array
	 */
	public function update(string $customer, array $data) : array
	{
		try{
			$customer = $this->_shopify->call(
				[
					'URL' => '/admin/customers/'.$customer.'.json',
					'METHOD' => 'PUT',
					'DATA' => [
						'customer' => $data
					]
				]
			);

			return ['status' => true, 'customer' => $customer->customer];
		} catch (\Exception $exception) {
			return ['status' => false, 'message' => $exception->getMessage()];
		}
	}

	/**
	 * @param string $customer
	 * @return array
	 */
	public function delete(string $customer): array
	{
		try{
			$customer = $this->_shopify->call(
				[
					'URL' => '/admin/customers/'.$customer.'.json',
					'METHOD' => 'DELETE',
				]
			);
			return ['status' => true];
		} catch (\Exception $exception)
		{
			return ['status' => false, 'message' => $exception->getMessage()];
		}
	}

}