<?php


namespace App\ShopifyApi;


use App\Services\ShopifyServices;
use Contract\ShopifyAPI\PriceRuleApiInterface;

class PriceRuleApi extends ShopifyServices implements PriceRuleApiInterface
{
    /**
     * @param $discountCode
     * @return array
     */
    public function createPriceRule($discountCode) : array
    {
        try{
            $priceRule = $this->_shopify->call(
                [
                    'URL' => '/admin/price_rules.json',
                    'METHOD' => 'POST',
                    'DATA' => [
                        'price_rule' => [
                            'title' => $discountCode,
                            'target_type' => 'line_item',
                            'target_selection' => 'all',
                            'allocation_method' => 'across',
                            'value_type' => 'percentage',
                            'value' => -10,
                            'once_per_customer' => true,
                            'usage_limit' => null,
                            'customer_selection' => 'all',
                            'prerequisite_subtotal_range' => null,
                            'prerequisite_shipping_price_range' => null,
                            'starts_at' => "2017-07-26T15:10:12Z",
                            'ends_at' => null,
                        ]
                    ]
                ]
            );

            $discountCode = $this->createDiscountCodes($priceRule->price_rule->id, $discountCode);
            if( ! $discountCode['status'])
                return ['status' => false, 'message' => $discountCode['message']];

            return ['status' => true, compact('priceRule'), compact('discountCode')];
        } catch (\Exception $exception)
        {
            return ['status' => false, 'message' => $exception->getMessage()];
        }
    }

    /**
     * @param $idPriceRule
     * @param $discountCode
     * @return array
     */
    public function createDiscountCodes($idPriceRule, $discountCode)
    {
        try{
            $priceRule = $this->_shopify->call(
                [
                    'URL' => '/admin/price_rules/'.$idPriceRule.'/discount_codes.json',
                    'METHOD' => 'POST',
                    'DATA' => [
                        'discount_code' => [
                            'code' => $discountCode,
                        ]
                    ]
                ]
            );


            return ['status' => true, 'priceRule' => $priceRule];
        } catch (\Exception $exception)
        {
            //If create discount error delete price rule
            if( ! $this->deletePriceRule($idPriceRule))
                return ['status' => false, 'message' => 'Cannot delete price rule'];

            return ['status' => false, 'message' => $exception->getMessage()];
        }
    }

    /**
     * @param $idPriceRule
     * @return bool
     */
    public function deletePriceRule($idPriceRule)
    {
        try{
            $this->_shopify->call([
                'URL' => '/admin/price_rules/'.$idPriceRule.'.json',
                'METHOD' => 'DELETE'
            ]);

            return true;
        } catch (\Exception $exception)
        {
            return false;
        }

    }

    /**
     * @return array
     */
    public function all()
    {
        try{
            $priceRule = $this->_shopify->call([
                'URL' => 'price_rules.json',
                'METHOD' => 'GET'
            ]);

            return $priceRule;
        } catch (\Exception $exception)
        {
            return ['status' => false, 'message' => $exception->getMessage()];
        }
    }
}