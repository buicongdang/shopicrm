<?php
/**
 * Created by PhpStorm.
 * User: buicongdang
 * Date: 9/6/17
 * Time: 3:47 PM
 */

namespace App\ShopifyApi;


use App\Services\ShopifyServices;

class MetaFieldApi extends ShopifyServices
{
    /**
     * @param $nameSpace
     * @param $key
     * @param $valueMetaField
     * @return array
     */
    public function addMetaField($nameSpace, $key, $valueMetaField)
    {
        try{
            $metaField = $this->_shopify->call([
                'URL' => 'metafields.json',
                'METHOD' => 'POST',
                'DATA' => [
                    'metafield' => [
                        'namespace'  => $nameSpace,
                        'key'        => $key,
                        'value'      => $valueMetaField,
                        'value_type' => 'string'
                    ]
                ]
            ]);
            return ['status' => true, 'metaField' => $metaField->metafield];
        }catch (\Exception $exception)
        {
            return ['status' => false, 'message' => $exception->getMessage()];
        }
    }

    /**
     * @return array
     */
    public function allMetaField()
    {
        try{
            $metaField = $this->_shopify->call([
                'URL' => 'metafields.json',
                'METHOD' => 'GET'
            ]);
            return ['status' => true, 'metaField' => $metaField->metafields];
        } catch (\Exception $exception)
        {
            return ['status' => false, 'message' => $exception->getMessage()];
        }
    }
}