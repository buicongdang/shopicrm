<?php

namespace App\ShopifyApi;


use App\Services\ShopifyServices;
use Contract\ShopifyAPI\OrdersApiInterface;

class OrdersApi extends ShopifyServices implements OrdersApiInterface
{
	/**
	 * OrdersApi constructor.
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * @param array $field
	 * @param int $page
	 * @param int $limit
	 * @param string $status
	 * @return array
	 */
	public function all(array $field = [], int $page = 1, int $limit = 250, $status = 'any') : array
	{
		try{
			$field = implode(',', $field);
			$orders = $this->_shopify->call(
				[
					'URL' => 'orders.json',
					'METHOD' => 'GET',
					'DATA' => [
						'limit' => $limit,
						'page' => $page,
						'fields' => $field,
						'financial_status' => $status
					]
				]
			);
			return ['status' => true, 'orders' => $orders->orders];
		} catch (\Exception $exception)
		{
			return ['status' => false, 'message' => $exception->getMessage()];
		}
	}

	/**
	 * @param array $field
	 * @param string $order
	 * @return array
	 */
	public function detail(array $field = [], string $order) : array
	{
		try{
			$field = implode(',', $field);
			$order = $this->_shopify->call(
				[
					'URL' => 'orders/'.$order.'.json',
					'METHOD' => 'GET',
					'DATA' => [
						'fields' => $field
					]
				]
			);
			return ['status' => true, 'order' => $order->order];
		} catch (\Exception $exception)
		{
			return ['status' => false, 'message' => $exception->getMessage()];
		}
	}

	/**
	 * @param string $status
	 * @return array
	 */
	public function count(string $status = 'any') : array
	{
		try{
			$count = $this->_shopify->call(
				[
					'URL' => '/admin/orders/count.json',
					'METHOD' => 'GET',
					'DATA' => [
						'financial_status' => $status
					]
				]
			);
			return ['status' => true, 'count' => $count->count];

		} catch (\Exception $exception)
		{
			return ['status' => false, 'message' => $exception->getMessage()];
		}
	}

	/**
	 * @param array $data
	 * @return array
	 */
	public function create(array $data) : array
	{
		try{
			$order = $this->_shopify->call(
				[
					'URL' => '/admin/orders.json',
					'METHOD' => 'POST',
					'DATA' => [
						'order' => $data
					]
				]
			);
			return ['status' => true, 'order' => $order->order];
		} catch (\Exception $exception)
		{
			return ['status' => false, 'message' => $exception->getMessage()];
		}
	}

	/**
	 * @param string $order
	 * @param array $data
	 * @return array
	 */
	public function update(string $order, array $data) : array
	{
		try{
			$order = $this->_shopify->call(
				[
					'URL' => '/admin/orders/'.$order.'.json',
					'METHOD' => 'PUT',
					'DATA' => [
						'order' => $data
					]
				]
			);

			return ['status' => true, 'order' => $order->order];
		} catch (\Exception $exception) {
			return ['status' => false, 'message' => $exception->getMessage()];
		}
	}

	/**
	 * @param string $order
	 * @return array
	 */
	public function delete(string $order): array
	{
		try{
			$order = $this->_shopify->call(
				[
					'URL' => '/admin/orders/'.$order.'.json',
					'METHOD' => 'DELETE',
				]
			);
			return ['status' => true];
		} catch (\Exception $exception)
		{
			return ['status' => false, 'message' => $exception->getMessage()];
		}
	}

}