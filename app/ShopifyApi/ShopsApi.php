<?php
/**
 * Created by PhpStorm.
 * User: buicongdang
 * Date: 8/19/17
 * Time: 8:33 PM
 */

namespace App\ShopifyApi;


use App\Services\ShopifyServices;
use Contract\ShopifyAPI\ShopsApiInterface;

class ShopsApi extends ShopifyServices implements ShopsApiInterface
{
    public function get()
    {
        try{
            $shop = $this->_shopify->call([
                'URL' => 'shop.json',
                'METHOD' => 'GET'
            ]);
            return ['status' => true, 'shop' => $shop->shop];
        } catch (\Exception $exception)
        {
            return ['status' => false, 'message' => $exception->getMessage()];
        }
    }
}