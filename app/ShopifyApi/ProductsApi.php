<?php

namespace App\ShopifyApi;

use App\Services\ShopifyServices;
use Contract\ShopifyAPI\ProductsApiInterface;

/**
 * Class ProductsApi
 * @package App\ShopifyApi
 */
class ProductsApi extends ShopifyServices implements ProductsApiInterface
{
    /**
     * @param array $field
     * @param int $page
     * @param int $limit
     * @param string $status
     * @return array
     */
    public function all(array $field = [], int $page = 1, int $limit = 250, $status = 'published') : array
    {
        try{
            $field = implode(',', $field);
            $products = $this->_shopify->call(
                [
                    'URL' => 'products.json',
                    'METHOD' => 'GET',
                    'DATA' => [
                        'limit' => $limit,
                        'page' => $page,
                        'fields' => $field,
                        'published_status' => $status
                    ]
                ]
            );
            return ['status' => true, 'products' => $products->products];
        } catch (\Exception $exception)
        {
            return ['status' => false, 'message' => $exception->getMessage()];
        }
    }

    /**
     * @param array $field
     * @param string $product
     * @return array
     */
    public function detail(array $field = [], string $product) : array
    {
        try{
            $field = implode(',', $field);
            $product = $this->_shopify->call(
                [
                    'URL' => 'products/'.$product.'.json',
                    'METHOD' => 'GET',
                    'DATA' => [
                        'fields' => $field
                    ]
                ]
            );
            return ['status' => true, 'product' => $product->product];
        } catch (\Exception $exception)
        {
            return ['status' => false, 'message' => $exception->getMessage()];
        }
    }

    /**
     * @param string $status
     * @return array
     */
    public function count(string $status = 'published') : array
    {
        try{
            $count = $this->_shopify->call(
                [
                    'URL' => '/admin/products/count.json',
                    'METHOD' => 'GET',
                    'DATA' => [
                        'published_status' => $status
                    ]
                ]
            );
            return ['status' => true, 'count' => $count->count];

        } catch (\Exception $exception)
        {
            return ['status' => false, 'message' => $exception->getMessage()];
        }
    }

    /**
     * @param array $data
     * @return array
     */
    public function create(array $data) : array
    {
        try{
            $product = $this->_shopify->call(
                [
                    'URL' => '/admin/products.json',
                    'METHOD' => 'POST',
                    'DATA' => [
                        'product' => $data
                    ]
                ]
            );
            return ['status' => true, 'product' => $product->product];
        } catch (\Exception $exception)
        {
            return ['status' => false, 'message' => $exception->getMessage()];
        }
    }

    /**
     * @param string $product
     * @param array $data
     * @return array
     */
    public function update(string $product, array $data) : array
    {
        try{
            $product = $this->_shopify->call(
                [
                    'URL' => '/admin/products/'.$product.'.json',
                    'METHOD' => 'PUT',
                    'DATA' => [
                        'product' => $data
                    ]
                ]
            );

            return ['status' => true, 'product' => $product->product];
        } catch (\Exception $exception) {
            return ['status' => false, 'message' => $exception->getMessage()];
        }
    }

    /**
     * @param string $product
     * @return array
     */
    public function delete(string $product): array
    {
        try{
            $product = $this->_shopify->call(
                [
                    'URL' => '/admin/products/'.$product.'.json',
                    'METHOD' => 'DELETE',
                ]
            );
            return ['status' => true];
        } catch (\Exception $exception)
        {
            return ['status' => false, 'message' => $exception->getMessage()];
        }
    }
}