<?php

namespace App\Providers;

use App\Repository\CustomersRepository;
use App\Services\ShopifyServices;
use Contract\Repository\CustomersRepositoryInterface;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
       ///echo env('API_KEY');
        //die;
        $this->app['request']->server->set('HTTPS','on');
        Schema::defaultStringLength(191);

    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {

    }
}
