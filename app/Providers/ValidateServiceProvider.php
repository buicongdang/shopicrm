<?php

namespace App\Providers;

use Illuminate\Support\Facades\Validator;
use Illuminate\Support\ServiceProvider;

class ValidateServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        Validator::extend('shopify_domain', function ($attribute, $value, $parameters, $validator) {
            if(preg_match('/^([A-Z]|[a-z]|[0-9]|\-)+.myshopify.com$/',$value))
                return true;
        });
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
