<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\View;
use Cartalyst\Sentinel\Native\Facades\Sentinel;

class ViewServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {

        View::addNamespace('campaign_mail', base_path().'/public');
        if($currentLogin = Sentinel::check())
        {
            View::share('currentLogin', $currentLogin);
        }
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
