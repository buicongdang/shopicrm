<?php

namespace App\Factory;


use App\ShopifyApi\CustomersApi;
use App\ShopifyApi\OrdersApi;
use App\ShopifyApi\PriceRuleApi;
use App\ShopifyApi\ProductsApi;
use App\ShopifyApi\ShopsApi;

class ShopifyApiFactory
{
    public function __construct()
    {

    }

    public static function productsFactory()
    {
        return new ProductsApi();
    }

    public static function shopFactory()
    {
        return new ShopsApi();
    }

	public static function ordersFactory()
	{
		return new OrdersApi();
	}

	public static function customersFactory()
	{
		return new CustomersApi();
	}

	public static function priceRuleFactory()
    {
        return new PriceRuleApi();
    }
}
