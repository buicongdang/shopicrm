<?php

namespace App\Factory;

use App\Repository\CampaignsRepository;
use App\Repository\CustomersRepository;
use App\Repository\MembershipRepository;
use App\Repository\OrdersRepository;
use App\Repository\ProductsRepository;
use App\Repository\ShopsRepository;
use App\Repository\UsersRepository;
use App\Services\ShopifyServices;

class RepositoryFactory
{
    /**
     * @return UsersRepository
     */
    public static function usersFactory()
    {
        return app(UsersRepository::class);
    }

    /**
     * @return CustomersRepository
     */
    public static function customersFactory()
    {
        return app(CustomersRepository::class);
    }

	/**
	 * @return MembershipRepository
	 */
	public static function membershipFactory()
	{
		return app(MembershipRepository::class);
	}

    /**
     * @return ProductsRepository
     */
	public static function productFactory()
    {
        return app(ProductsRepository::class);
    }

	/**
	 * @return OrdersRepository
	 */
	public static function orderFactory()
	{
		return app(OrdersRepository::class);
	}

    /**
     * @return ShopsRepository
     */
	public static function shopFactory()
    {
        return app(ShopsRepository::class);
    }

    /**
     * @param $accessToken
     * @param $shopDomain
     * @return ShopifyServices
     */
	public static function shopifyServiceFactory()
    {
        return new ShopifyServices();
    }

    /**
     * @return CampaignsRepository
     */
    public static function campaignFactory()
    {
        return app(CampaignsRepository::class);
    }
}