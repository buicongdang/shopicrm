<?php


namespace App\Repository;

use App\Models\ProductsModel;
use Contract\Repository\ProductsRepositoryInterface;

/**
 * Class ProductsRepository
 * @package App\Repository
 */
class ProductsRepository implements ProductsRepositoryInterface
{

    private $_model;

    public function __construct(ProductsModel $product)
    {
        $this->_model = $product;
    }

    /**
     * @param array $products
     * @return bool
     */
    public function addMultiProduct(array $products)
    {
        foreach ($products['products'] as $obj) {
            $this->addProduct((array)$obj);
        }
        return true;
    }


    /**
     * @param array $data
     *
     * @return mixed
     */
    public function addProduct(array $data = [])
    {
        $result = $this->convertData($data);
        $this->_model->create($result);
    }
    public function convertData($data)
    {
        $data['images'] = !empty($data['images'][0]->src) ? $data['images'][0]->src : "" ;
        return $data;
    }
}