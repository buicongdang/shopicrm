<?php

namespace App\Repository;


use App\Models\CampaignsAutoModel;
use Contract\Repository\CampaignsRepositoryInterface;
use Illuminate\Support\Facades\Lang;

class CampaignsRepository implements CampaignsRepositoryInterface
{
    private $_model;

    /**
     * CampaignsRepository constructor.
     * @param CampaignsAutoModel $campaign
     */
    public function __construct( CampaignsAutoModel $campaign )
    {
        $this->_model = $campaign;
    }
    /**
     * @param array $data
     * @return mixed
     */
    public function insert ( array $data = [] )
    {
        $result = array(
            'status'  => config( 'common.status.unpulish' ),
            'message' => Lang::get( 'customer.failed' ),
        );
        $id = $this->_model->create($data)->id;
        if ( ! empty( $id ) ) {
            $result = array(
                'status'  => config( 'common.status.publish' ),
                'message' => Lang::get( 'customer.create_success' ),
                'data'    => $id
            );
        }
        return $result;
    }

    /**
     * @param array $data
     * @return mixed
     */
    public function update ( array $data = [] )
    {
        // TODO: Implement update() method.
    }
}