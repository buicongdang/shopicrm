<?php


namespace App\Repository;

use App\Models\MembershipModel;
use Contract\Repository\MembershipRepositoryInterface;
use Illuminate\Support\Facades\Lang;

/**
 * Class MembershipRepository
 * @package App\Repository
 */
class MembershipRepository implements MembershipRepositoryInterface {

	private $_model;

	public function __construct( MembershipModel $membership_model ) {
		$this->_model = $membership_model;
	}

	/**
	 * Get list membership by params
	 *
	 * @param array $params
	 *
	 * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
	 */
	public function all( array $params = array() ) {
		$perPage = ! empty( $params['limit'] ) ? $params['limit'] : config( 'common.pagination' );

		$result = $this->_model->orderBy( 'id', 'desc' )->paginate( $perPage );
		if ( ! empty( $result->total() ) ) {
			foreach ( $result as &$v ) {
				$v = $this->detail( $v->id );
			}
		}

		return $result;
	}

	/**
	 * Get membership detail by id
	 *
	 * @param $id
	 *
	 * @return \Illuminate\Database\Eloquent\Model|null|static
	 */
	public function detail( $id ) {
		$result = $this->_model->where( 'id', $id )->first();
		if ( $result ) {
			$image_link = '';
			if ( $result->image ) {
				$image_link = asset( '/public/storage/membership-images/' . $result->image );
			}

			$result->image_link = $image_link;
		}

		return $result;
	}

	/**
	 * Insert membership
	 *
	 * @param $data
	 *
	 * @return array
	 */
	public function insert( $data ): array {
		$result = array(
			'status'  => config( 'common.status.unpulish' ),
			'message' => Lang::get( 'membership.failed' ),
		);

		$id = $this->_model->insertGetId( $data );

		if ( ! empty( $id ) ) {
			$result = array(
				'status'  => config( 'common.status.publish' ),
				'message' => Lang::get( 'membership.create_success' ),
				'data'    => $id
			);
		}

		return $result;
	}

	/**
	 * Update membership
	 *
	 * @param $data
	 *
	 * @return array
	 */
	public function update( $data ): array {

		$result = array(
			'status'  => config( 'common.status.unpublish' ),
			'message' => Lang::get( 'membership.failed' ),
		);
		if ( ! empty( $data['id'] ) ) {
			$update = $this->_model->where( 'id', $data['id'] )->update( $data );

			if ( ! empty( $update ) ) {
				$result = array(
					'status'  => config( 'common.status.publish' ),
					'message' => Lang::get( 'membership.update_success' ),
				);
			}
		}

		return $result;
	}


	public function delete( int $id = 0 ): array {
		$result = array(
			'status'  => config( 'common.status.unpulish' ),
			'message' => Lang::get( 'membership.failed' ),
		);

		if ( $id ) {
			$delete = $this->_model->where( 'id', $id )->delete();

			if ( $delete ) {
				$result = array(
					'status'  => config( 'common.status.publish' ),
					'message' => Lang::get( 'membership.delete_success' ),
				);
			}
		}

		return $result;
	}
}