<?php

namespace App\Repository;

use App\Models\OrdersModel;
use Contract\Repository\OrdersRepositoryInterface;
use Illuminate\Support\Facades\DB;

/**
 * Class OrdersRepository
 * @package App\Repository
 */
class OrdersRepository implements OrdersRepositoryInterface {

	private $_model;

	public function __construct( OrdersModel $order ) {
		$this->_model = $order;
	}


	/**
	 * Add multi order
	 *
	 * @param $orders
	 */
	public function addMultiOrder($orders){
		if(!empty($orders)){
			foreach ($orders as $order){
				$save = $this->addOrders($order);

				if(!$save) return false;
			}
		}

		return true;
	}

	/**
	 * Add order
	 *
	 * @param $order
	 *
	 * @return bool
	 */
	public function addOrders( $order ): bool {
		//DB::transaction(function()  use ($order) {
			$data_save = array(
				'id'                 => $order->id,
				'shop_id'            => !empty($order->shop_id) ? $order->shop_id : 0,
				'email'              => $order->email,
				'total_price'        => intval( $order->total_price ),
				'currency'           => $order->currency,
				'fulfillment_status' => $order->fulfillment_status,
				'financial_status'   => $order->financial_status,
				'products'           => '',//serialize($order->line_items),
				'customer'           => $order->customer->id,
				'created_at'         => date( 'Y-m-d H:i:s', strtotime( $order->created_at ) ),
				'updated_at'         => date( 'Y-m-d H:i:s', strtotime( $order->updated_at ) ),
			);

			$add = $this->_model->insert( $data_save );

			if ( $add ) {
				return true;
			} else {
				return false;
			}

		//});
	}
}
