<?php
//declare(strict_types=1);

namespace App\Repository;

use Cartalyst\Sentinel\Laravel\Facades\Reminder;
use Cartalyst\Sentinel\Native\Facades\Sentinel;
use Contract\Repository\UsersRepositoryInterface;
use Illuminate\Support\Facades\Lang;

class UsersRepository implements UsersRepositoryInterface
{
    /**
     * @param array $data
     * @return array
     */
    public function login( $data = ['email', 'password']) : array
    {
        try
        {
            $auth = Sentinel::authenticateAndRemember($data);
            if( ! $auth)
            {
                return ['status' => false, 'message' => Lang::get('auth.login_failed')];
            }

            return ['status' => true, 'message' => Lang::get('auth.login_trued')];

        }
        catch (\Exception $exception)
        {
            $errors = $exception->getMessage();
            return ['status' => false, 'message' => $errors];
        }
    }

    /**
     * @param array $data
     * @return array
     */
    public function register($data = ['email', 'password']) : array
    {
        //Check email exist
        $user = Sentinel::findByCredentials(['login' => $data['email']]);
        if($user)
            return ['status' => false, 'message' => Lang::get('auth.email_exist')];

        //Register user if success true else false
        $register  = Sentinel::register($data, true);
        if( ! $register)
            return ['status' => false, 'message' => Lang::get('auth.register_failed')];

        return ['status' => true, 'message' => Lang::get('auth.register_trued')];
    }

    /**
     * @param string $email
     * @return array
     */
    public function forgotPassword(string $email) : array
    {
        $credentials = [
            'login' => $email
        ];
        $user = Sentinel::findByCredentials($credentials);
        echo '<pre>';
        var_dump($user->getAttributes());
        echo '</pre>';
        return [];

    }

    /**
     * @return bool
     */
    public function logout() : bool
    {
        return Sentinel::logout();
    }

}
