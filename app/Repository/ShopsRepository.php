<?php
/**
 * Created by PhpStorm.
 * User: snakexxx
 * Date: 02/08/2017
 * Time: 14:54
 */

namespace App\Repository;


use App\Models\ShopsModel;
use Contract\Repository\ShopsRepositoryInterface;

/**
 * Class ShopsRepository
 * @package App\Repository
 */
class ShopsRepository implements ShopsRepositoryInterface
{
    /**
     * @var ShopsModel
     */
    private $_shopModel;

    /**
     * ShopsRepository constructor.
     *
     * @param ShopsModel $shop
     */
    public function __construct(ShopsModel $shop)
    {
        $this->_shopModel = $shop;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function all()
    {
        return $this->_shopModel->all();
    }

    public function delete()
    {

    }

    public function detail(array $field)
    {
        return $this->_shopModel->when($field)->firstOrFail();
    }

    public function edit()
    {
        // TODO: Implement edit() method.
    }

    public function insert(array $data)
    {
        try{
            $shopModel = $this->_shopModel->findOrNew($data['id']);

            foreach ($this->_shopModel->getFillable() as $k=>$v)
            {
                $shopModel->setAttribute($v, $data[$v]);
            }

            $isSave = $shopModel->save();

            return ['status' => $isSave];
        } catch (\Exception $exception)
        {
            return ['status' => false, 'message' => $exception->getMessage()];
    }
}