<?php


namespace App\Repository;

use App\Factory\RepositoryFactory;
use App\Models\CustomersModel;
use Contract\Entity\EloquentEntity;
use Contract\Repository\CustomersRepositoryInterface;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Lang;

/**
 * Class CustomersRepository
 * @package App\Repository
 */
class CustomersRepository implements CustomersRepositoryInterface
{
    private $_model;

    /**
     * CustomersRepository constructor.
     * @param CustomersModel $customers
     */
    public function __construct(CustomersModel $customers)
    {
        $this->_model = $customers;
    }

	/**
	 * Insert multi customers
	 *
	 * @param $customers
	 *
	 * @return bool
	 */
    public function addMultiCustomer($customers){
	    if(!empty($customers)){
		    foreach ($customers as $customer){

		    	// convert data form api shopify to array
		    	if(is(is_object($customer))){
		    		$data_convert = $this->convertFromApi($customer);
				    $customer = (array)$data_convert;
			    }

			    $save = $this->insert($customer);

			    if(!$save['status']) return false;
		    }
	    }

	    return true;
    }


	/**
	 * Convert data from API ShopiFy
	 *
	 * @param object $data
	 *
	 * @return mixed
	 */
    public function convertFromApi($data){
    	$data_rt = (object) array();

	    $data_rt->id_customer = $data->id;
	    $data_rt->fist_name = $data->first_name;
	    $data_rt->last_name = $data->last_name;
	    $data_rt->shop_id = !empty($data->shop_id) ? $data->shop_id : 0;
	    $data_rt->status = !empty($data->verified_email) ? '1' : '0';
	    $data_rt->accepts_marketing = 1;

	    $data_rt->email_primary = $data->email;
	    //$data_rt->emails = json_encode(array($data->email));

	    $data_rt->phone_primary = $data->phone;
	    //$data_rt->phones = json_encode(array($data->phone));

	    if($data->default_address->province){
		    $data_rt->province = $data->default_address->province;
	    }

	    if($data->default_address->city){
		    $data_rt->city = $data->default_address->city;
	    }

	    if($data->default_address->country){
		    $data_rt->country = $data->default_address->country;
	    }

	    if($data->default_address->country_code){
		    $data_rt->country_code = $data->default_address->country_code;
	    }

	    if($data->default_address->address1){
		    $data_rt->address = $data->default_address->address1;
	    }

	    return $data_rt;
    }

    /**
     * @return mixed
     */
    public function insert(array $data = [])
    {
        $result = array(
            'status'  => config( 'common.status.unpulish' ),
            'message' => Lang::get( 'customer.failed' ),
        );
        $id = $this->_model->create($data)->id;
        if ( ! empty( $id ) ) {
            $result = array(
                'status'  => config( 'common.status.publish' ),
                'message' => Lang::get( 'customer.create_success' ),
                'data'    => $id
            );
        }
        return $result;

    }

    /**
     * @return mixed
     */
    public function delete()
    {
        // TODO: Implement delete() method.
    }

    /**
     * @return mixed
     */
    public function all()
    {
        return $this->_model->orderBy( 'id', 'desc' )->paginate(config('common.pagination'));
    }

    /**
     * @param array $order
     * @param array $filter
     * @return mixed
     */
    public function filter (array $order = [], array $filter = [] )
    {
        $table = DB::table($this->_model->getTable());
        if(isset($filter['name']))
        {
            $table->where('name', 'like', '%'.$filter['name'].'%');
        }
        if(isset($order['sort']) && isset($order['order']))
        {
            $table->orderBy($order['sort'], $order['order']);
        }

        return $table->paginate(config('common.pagination'));
    }
    /**
     * @return mixed
     */
    public function detail($id)
    {
        $result = $this->_model->find($id);
        return $result;
    }

    /**
     * @param int $id
     * @param array $data
     * @return mixed
     */
    public function update ( $id, array $data)
    {
        $result = $this->_model->find($id);
        if($result) {
            $result->update($data);
            return $result;
        }
        return false;
    }

    /**
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public function getMemberLevel()
    {
        $membershipRepo = RepositoryFactory::membershipFactory();
        $list = $membershipRepo->all();
        return $list;
    }
}