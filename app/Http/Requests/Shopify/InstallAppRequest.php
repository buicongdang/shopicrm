<?php

namespace App\Http\Requests\Shopify;

use Illuminate\Foundation\Http\FormRequest;

class InstallAppRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'shopDomain' => 'required|shopify_domain'
        ];
    }


    public function messages()
    {
        return [
            'shopDomain.shopify_domain' => 'You must input url yourshop.myshopify.com. Ex: alireviews.myshopify.com'
        ];
    }
}
