<?php
/**
 * Created by PhpStorm.
 * User: buicongdang
 * Date: 8/19/17
 * Time: 9:27 AM
 */

namespace App\Http\Requests\Shopify;


use Illuminate\Foundation\Http\FormRequest;

class AuthAppRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [

        ];
    }
}