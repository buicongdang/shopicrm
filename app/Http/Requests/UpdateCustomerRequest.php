<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateCustomerRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'first_name'    => 'bail|required|string',
            'last_name'     => 'bail|required|string',
            'email_primary' => 'bail|required|email',
            'phone_primary' => 'bail|required|string',
            'birth_day'     => 'bail|required|date',
            'member_level'  => 'bail|required|integer',
            'address'       => 'bail|required|string',
        ];
    }
}
