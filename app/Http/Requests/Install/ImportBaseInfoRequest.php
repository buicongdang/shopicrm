<?php
/**
 * Created by PhpStorm.
 * User: buicongdang
 * Date: 8/20/17
 * Time: 9:26 AM
 */

namespace App\Http\Requests\Install;


use Illuminate\Foundation\Http\FormRequest;

class ImportBaseInfoRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'id' => 'required',
            'name' => 'required',
            'domain' => 'required',
            'email' => 'bail|required|email',
            'shop_owner' => 'required',
            'plan_name' => 'required',
            'myshopify_domain' => 'required',
            'shop_created_at' => 'required',
            'password' => 'required|min:6'
        ];
    }
}