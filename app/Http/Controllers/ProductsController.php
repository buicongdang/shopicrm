<?php

namespace App\Http\Controllers;

use App\Factory\RepositoryFactory;
use App\Http\Controllers\Base\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\URL;

/**
 * Class ProductsController
 * @package App\Http\Controllers
 */
class ProductsController extends Controller
{
    private $_productRepo;

    /**
     * ProductsController constructor.
     * @param RepositoryFactory $factory
     */
    public function __construct (RepositoryFactory $factory)
    {
        $this->_productRepo = $factory::productFactory();
    }

    public function importProduct(){
        $json = File::get(resource_path().'/datatest/product.json');
        $data = json_decode($json);
        $this->_productRepo->addMultiProduct((array)$data);
    }
}
