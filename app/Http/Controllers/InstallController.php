<?php

namespace App\Http\Controllers;

use App\Factory\RepositoryFactory;
use App\Factory\ShopifyApiFactory;
use App\Http\Controllers\Base\ShopiCrmController;
use App\Http\Requests\Install\ImportBaseInfoRequest;
use App\ShopifyApi\ShopifyApiBase;
use DaveJamesMiller\Breadcrumbs\Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;
use function Sodium\compare;

class InstallController extends ShopiCrmController
{
    /**
     * @var \App\ShopifyApi\ShopsApi
     */
    private $_shopApi;

    /**
     * @var \Illuminate\Foundation\Application|mixed
     */
    private $_shopRepo;

    /**
     * @var \App\Repository\UsersRepository
     */
    private $_userRepo;

    /**
     * InstallController constructor.
     */
    public function __construct()
    {
        $this->_shopRepo = RepositoryFactory::shopFactory();
        $this->_shopApi = ShopifyApiFactory::shopFactory();
        $this->_userRepo = RepositoryFactory::usersFactory();
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View|string
     */
    public function importBaseInfo()
    {
        try{
            $this->_shopApi->getAccessToken(session('accessToken'), session('shopDomain'));
            $shop = $this->_shopApi->get();

            return view('install.import_base_info', ['shop' => $shop['shop']]);
        } catch (\Exception $exception)
        {
            return $exception->getMessage();
        }
    }

    /**
     * @param ImportBaseInfoRequest $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function importBaseInfoHandle(ImportBaseInfoRequest $request)
    {
        $data = $request->all();
        $data['shop_created_at'] = date('Y-m-d H:i:s', strtotime($data['shop_created_at']));
        //Save user info register
        $register = $this->_userRepo->register(['email' => $data['email'], 'password' => $data['password']]);
        if( ! $register['status'])
            return \redirect(route('install.importBaseInfo'))->with('error', $register['message']);

        //Save shop info
        $isSaveShop = $this->_shopRepo->insert($data);
        if( ! $isSaveShop['status'])
        {
            return \redirect(route('install.importBaseInfo'))->with('error', $isSaveShop['message']);
        }

        return \redirect(route('install.importApi'));
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function importApi()
    {
        return view('install.import_api');
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function addShop()
    {
        return view('install.addShop');
    }
}
