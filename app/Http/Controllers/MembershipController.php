<?php

namespace App\Http\Controllers;

use App\Factory\RepositoryFactory;
use App\Http\Controllers\Base\Controller;
use App\Http\Requests\MembershipRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;

class MembershipController extends Controller {

	private $_membershipRepo;


	public function __construct( RepositoryFactory $factory ) {
		parent::__construct();

		$this->_membershipRepo = $factory::membershipFactory();
	}

	/**
	 * List membership
	 *
	 * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
	 */
	public function all() {
		$list = $this->_membershipRepo->all();

		return view( 'membership.list', [ 'list' => $list ] );
	}

	/**
	 * Page create membership
	 *
	 * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
	 */
	public function create() {
		return view( 'membership.create' );
	}

	/**
	 * Handle create membership
	 *
	 * @param MembershipRequest $request
	 *
	 * @return mixed
	 */
	public function createHandle( MembershipRequest $request ) {
		$data = $request->all();
		unset( $data['_token'] );
		$data['created_at'] = date( 'Y-m-d H:i:s', time() );
		$data['image']      = '';
		if ( $request->hasFile( 'image' ) ) {
			$image     = $request->file( 'image' );
			$imageName = time() . '.' . $image->getClientOriginalExtension();
			$request->file( 'image' )->move(
				storage_path( 'app/public/membership-images' ), $imageName
			);
			$data['image'] = $imageName;
		}

		$insert = $this->_membershipRepo->insert( $data );
		if ( ! $insert['status'] ) {
			return Redirect::route( 'membership.create' )->with( 'error', $insert['message'] );
		}

		return Redirect::route( 'membership.update', [ 'id' => $insert['data'] ] )->with( 'success', $insert['message'] );
	}

	/**
	 * Page update membership
	 *
	 * @param $id
	 *
	 * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
	 */
	public function update( $id ) {
		$membership_info = $this->_membershipRepo->detail( $id );

		return view( 'membership.update', [ 'membership_info' => $membership_info ] );
	}

	/**
	 * Handle update membership
	 *
	 * @param MembershipRequest $request
	 *
	 * @return mixed
	 */
	public function updateHandle( MembershipRequest $request ) {
		$data = $request->all();

		unset( $data['_token'] );
		$data['updated_at'] = date( 'Y-m-d H:i:s', time() );
		if ( $request->hasFile( 'image' ) ) {
			$image     = $request->file( 'image' );
			$imageName = time() . '.' . $image->getClientOriginalExtension();
			$request->file( 'image' )->move(
				storage_path( 'app/public/membership-images' ), $imageName
			);
			$data['image'] = $imageName;
		}

		$update = $this->_membershipRepo->update( $data );
		if ( ! $update['status'] ) {
			return Redirect::route( 'membership.update', [ 'id' => $data['id'] ] )->with( 'error', $update['message'] );
		}

		return Redirect::route( 'membership.update', [ 'id' => $data['id'] ] )->with( 'success', $update['message'] );
	}

	/**
	 * Ajax delete membership
	 *
	 * @param Request $request
	 */
	public function ajaxDelete( Request $request ) {
		if ( $request->ajax() ) {
			$result = $this->_membershipRepo->delete( $request['id'] );
			echo json_encode( $result );
			exit();
		}
	}

}