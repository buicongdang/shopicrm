<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Base\Controller;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;

class StripeController extends Controller
{
    /**
     * StripeController constructor.
     */
    public function __construct()
    {
        $this->user = Auth::user();

    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index ()
    {
        return view('stripe.card');
    }

    /**
     * @param Request $request
     */
    public function subscription ( Request $request )
    {
        $token = Input::get('stripeToken');
        $plan = $request->input('plan');
        $me = $this->user;
        //$user = User::find(1);
        //$user->newSubscription('main', $plan)->create($token);
        try {
            // check already subscribed and if already subscribed with picked plan
            if ($me->subscribed('main') && !$me->subscribedToPlan($plan, 'main')) {

                // swap if different plan attempt
                $me->subscription('main')->swap($plan);

            } else {
                // Its new subscription

                // if user has a coupon, create new subscription with coupon applied
                if ($coupon = $request->get('coupon')) {

                    $me->newSubscription('main', $plan)
                        ->withCoupon($coupon)
                        ->create($token, [
                            'email' => $me->email
                        ]);

                } else {

                    // Create subscription
                    $me->newSubscription('main', $plan)->create($token, [
                        'email' => $me->email,
                        'description' => $me->name
                    ]);
                }

            }
        } catch (\Exception $e) {
            // Catch any error from Stripe API request and show
            return redirect()->back()->withErrors(['status' => $e->getMessage()]);
        }
        echo 'Done';
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function cancelSubscription(Request $request)
    {
        try {
            $request->user()->subscription('main')->cancel();
        } catch ( \Exception $e) {
            return redirect()->route('home')->with('status', $e->getMessage());
        }
        return redirect()->route('home')->with('status',
            'Your Subscription has been canceled.'
        );
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function resumeSubscription(Request $request)
    {
        try {
            $request->user()->subscription('main')->resume();
        } catch ( \Exception $e) {
            return redirect()->route('home')->with('status', $e->getMessage());
        }
        return redirect()->route('home')->with('status',
            'Glad to see you back. Your Subscription has been resumed.'
        );
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function updateCard ( Request $request )
    {
        if (!Auth::check())
            return Redirect::route("login");
        $user = $request->user();
        $token = $request->input('token');
        $user->updateCard($token);
    }
    public function updateSubscription(Request $request)
    {
        $user = $request->user();
        // get the plan
        $plan = $request->input('plan');
        // if a user is cancelled
        if ($user->subscribed('main') and $user->subscription('main')->onGracePeriod()) {
            if ($user->onPlan($plan)) {
                // resume the plan
                $user->subscription('main')->resume();
            } else {
                // resume and switch plan
                $user->subscription('main')->resume()->swap($plan);
            }
            // if not cancelled, and switch
        } else {
            // change the plan
            $user->subscription('main')->swap($plan);
        }
        return redirect('home')->with(['success' => 'Subscription updated.']);
    }
    // Check is subscribed
    public function checksubscribed ()
    {
        $is_subscribed = Auth::user()->subscribed('main');
        return $is_subscribed;
    }

    // If subscribed get the subscription
    public function getsub ()
    {
        $subscription = Auth::user()->subscription('main');
        return $subscription;
    }

}
