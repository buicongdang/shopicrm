<?php

namespace App\Http\Controllers;

use App\Factory\RepositoryFactory;
use App\Http\Controllers\Base\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\URL;

/**
 * Class OrdersController
 * @package App\Http\Controllers
 */
class OrdersController extends Controller
{
    private $_orderRepo;

    /**
     * OrdersController constructor.
     * @param RepositoryFactory $factory
     */
    public function __construct (RepositoryFactory $factory)
    {
        $this->_orderRepo = $factory::orderFactory();
    }

	/**
	 * Import orders
	 */
    public function importOrder(){
	    $json = File::get(resource_path().'/datatest/orders.json');
	    $data = json_decode($json);

	    $this->_orderRepo->addMultiOrder($data->orders);
    }
}
