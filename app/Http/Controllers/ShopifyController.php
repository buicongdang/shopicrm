<?php

namespace App\Http\Controllers;

use App\Factory\RepositoryFactory;
use App\Http\Requests\Shopify\AuthAppRequest;
use App\Http\Requests\Shopify\InstallAppRequest;
use App\Services\ShopifyServices;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;

class ShopifyController extends Controller
{
    /**
     * @var ShopifyServices
     */
    private $_shopifyService;

    /**
     * ShopifyController constructor.
     */
    public function __construct()
    {
        $this->_shopifyService = RepositoryFactory::shopifyServiceFactory();
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function installApp()
    {
        return view('shopify.install_app');
    }

    public function installAppHandle(InstallAppRequest $request)
    {
        $this->_shopifyService->setShopDomain($request->input('shopDomain'));
        $authURL = $this->_shopifyService->installURL();
        return \redirect($authURL);
    }

    public function authApp(AuthAppRequest $request)
    {
        $auth = $this->_shopifyService->authApp($request->all());
        if($auth['status'])
        {
            session(['accessToken' => $auth['accessToken'], 'shopDomain' => $request->input('shop')]);
            $shop = $this->_shopifyService->detail(compact($request->input('shop')));
//            var_dump($shop);
//            die;
            return \redirect(route('install.importBaseInfo'));
        }

        return $auth['message'];
    }
}
