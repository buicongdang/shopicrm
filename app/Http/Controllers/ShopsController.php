<?php

namespace App\Http\Controllers;

use App\Factory\RepositoryFactory;
use App\Http\Controllers\Base\Controller;
use Illuminate\Http\Request;

class ShopsController extends Controller
{
    public function index()
    {
        $all = RepositoryFactory::shopsFactory();
        return view('shops.index');
    }
}
