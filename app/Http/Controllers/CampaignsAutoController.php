<?php

namespace App\Http\Controllers;

use App\Factory\RepositoryFactory;
use App\Helpers\Helpers;
use App\Http\Controllers\Base\Controller;
use App\Repository\ShopsRepository;
use Illuminate\Http\Request;

class CampaignsAutoController extends Controller
{
    private $_memberShipRepo;
    private $_shop;
    private $_repo;

    public function __construct ()
    {
        $this->_repo = RepositoryFactory::campaignFactory();
        $this->_memberShipRepo = RepositoryFactory::membershipFactory();
        $this->_shop = RepositoryFactory::shopFactory();
    }

    public function home ()
    {
        $memberShips = $this->_memberShipRepo->all();
        return view('campaigns_auto.home', compact('memberShips'));
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function stepOne ()
    {
        $domain = session('shopDomain');
        $shop = $this->_shop->detail(['domain' => $domain]);
        return view('campaigns_auto.step_one',compact('shop'));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    function createHandle (Request $request)
    {
        $data = $request->all();
        $meta_date = $request->input('meta_date', []);
        $data['meta'] = Helpers::parseTojson('date' , $meta_date);
        $result = $this->_repo->insert($data);
        if (!$result['status']) {
            echo "loi tai ban ngu";
        }
        echo 'Thông thôi';
    }
}
