<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Base\Controller;
use Illuminate\Http\Request;

class EmailBuilderController extends Controller
{
    public function saveEmailTemplate(Request $request)
    {
        $data = $request->all();
        $content = $data['content'];
        $html = $data['html'];
        try{
            file_put_contents('test.blade.php', $html);
            return response()->json(['status' => true]);
        } catch (\Exception $exception)
        {
            return response()->json(['status' => false, 'message' => $exception->getMessage()]);
        }
    }
}
