<?php

namespace App\Http\Controllers;

use App\Factory\ShopifyApiFactory;
use App\Services\ShopifyServices;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use PhpParser\Node\Expr\Array_;

class SummaryController extends Controller
{
    private $_productsApi;
    private $_shopApi;
    public function __construct()
    {
        $this->_productsApi = ShopifyApiFactory::productsFactory();
        $this->_shopApi = ShopifyApiFactory::shopFactory();
    }

    public function dashboard(Request $request)
    {
        $field = ['id','title','handle'];
        $products = $this->_productsApi->all($field, 2, 1);
//        $product = $this->_productsApi->detail($field,'73824847411');
//        $count = $this->_productsApi->count();
//        var_dump($count);
//
//        var_dump($product);

//        $product = '8860533189';
//        $data = [
//            "title" =>  "Test input product api",
//            "body_html" => "<strong>Good snowboard!<\/strong>",
//        ];
//        $product = $this->_productsApi->create($data);
//        $product = $this->_productsApi->delete($product);
//        var_dump($product);
//        echo '<pre>';
//        var_dump($this->_shopApi->get());
//        echo '</pre>';
//        die;
        return view('summary.dashboard', compact($products));
    }
}
