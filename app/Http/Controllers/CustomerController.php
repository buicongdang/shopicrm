<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Base\Controller;
use App\Http\Requests\CustomerRequest;
use App\Http\Requests\UpdateCustomerRequest;
use App\Models\CustomersModel;
use App\Factory\RepositoryFactory;
use Faker\Provider\Image;
use Illuminate\Http\Request;
use Contract\Repository\CustomersRepositoryInterface;
use Illuminate\Support\Facades\Lang;

/**
 * Class CustomerController
 * @package App\Http\Controllers
 */
class CustomerController extends Controller
{

    private $_customerRepo;

    /**
     * CustomerController constructor.
     * @param RepositoryFactory $factory
     * @internal param CustomersRepositoryInterface $customerRepository
     */
    public function __construct ( RepositoryFactory $factory )
    {
        $this->_customerRepo = $factory::customersFactory();
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index ( Request $request )
    {
        $res = $request->all();

        $order = [
            'sort' => isset($res['sort']) ? $res['sort'] : 'created_at',
            'order' => isset($res['order']) ? $res['order'] : 'asc'
        ];

        $filter = [

        ];
        $customers = $this->_customerRepo->filter($order, $filter);
        return view('customers.list', array('listCustomer' => $customers));
    }

    /**
     * @param CustomerRequest $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function created ()
    {
        $listMemberLevel = $this->_customerRepo->getMemberLevel();
        return view('customers.created', array('listMember' => $listMemberLevel));
    }

    public function update ( $id )
    {
        $listMemberLevel = $this->_customerRepo->getMemberLevel();
        $customer = $this->_customerRepo->detail($id);
        return view('customers.update', array('listMember' => $listMemberLevel, 'customer' => $customer));
    }

    /**
     * @param CustomerRequest $request
     */
    public function createHandle ( CustomerRequest $request )
    {
        $data = $this->initData($request);
        $data['status'] = config('common.status.publish');
        $result = $this->_customerRepo->insert($data);
        if (!$result['status']) {
            return redirect()->route('customer.created')->with('error', $result['message']);
        }
        return redirect()->route('customer.update', ['id' => $result['data']])->with('success', $result['message']);
    }

    public function updateHandle ( UpdateCustomerRequest $request )
    {
        $data = $this->initData($request);
        $result = $this->_customerRepo->update($data['id'], $data);
        if (!$result) {
            return redirect()->route('customer.update', ['id' => $data['id']])->with('error', Lang::get('customer.failed'));
        }
        return redirect()->route('customer.update', ['id' => $data['id']])->with('success', Lang::get('customer.update_success'));
    }

    public function initData ( $request )
    {
        $data = $request->all();
        $data['birth_day'] = date('Y-m-d', strtotime($data['birth_day']));
        // parse array email to json
        $emails = $request->input('emails', []);
        $phones = $request->input('phones', []);
        $data['emails'] = $this->parseTojson('emails', $emails);
        $data['phones'] = $this->parseTojson('phones', $phones);
        //end
        if ($request->hasFile('avatar')) {
            $avatar = $request->file('avatar');
            $imageName = time() . '.' . $avatar->getClientOriginalExtension();
            $request->file('avatar')->move(
                storage_path('app/public/avatar'), $imageName
            );
            $data['avatar'] = $imageName;
        }
        return $data;
    }

    public function getCustomers ( $id = null )
    {
        if ($id != null) {
            $cus_info = $this->_customerRepo->all();
        } else {
            $cus_info = $this->_customerRepo->detail($id);
        }
        return $cus_info;
    }

    public function parseTojson ( $name, $data )
    {
        $record = array();
        foreach ($data as $key => $value) {
            if (!empty($value))
                $record[$key][$name] = $value;
        }
        return json_encode($record);
    }
}
