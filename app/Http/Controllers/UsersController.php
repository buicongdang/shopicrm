<?php

namespace App\Http\Controllers;

use App\Factory\RepositoryFactory;
use App\Factory\ShopifyApiFactory;
use App\Http\Controllers\Base\Controller;
use App\Http\Requests\Users\ForgotPasswordRequest;
use App\Http\Requests\Users\LoginRequest;
use App\Http\Requests\Users\RegisterRequest;
use App\Repository\UsersRepository;
use Illuminate\Support\Facades\Redirect;

class UsersController extends Controller
{
    /**
     * @var UsersRepository
     */
    private $_userRepository;

    private $_shopApi;

    private $_shopRepo;

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function login()
    {
        return view('users.login');
    }

    /**
     * UsersController constructor.
     */
    public function __construct()
    {
        $this->_userRepository = RepositoryFactory::usersFactory();
        $this->_shopApi = ShopifyApiFactory::shopFactory();
        $this->_shopRepo = RepositoryFactory::shopFactory();
    }

    /**
     * @param LoginRequest $request
     * @return mixed
     */
    public function loginHandle(LoginRequest $request)
    {
        $login = $this->_userRepository->login($request->only(['email', 'password']));
        if( ! $login['status'])
        {
            return Redirect::route('users.login')->with('error', $login['message']);
        }

        //Save token
        $shopInfo = $this->_shopRepo->detail(['email' => $request->input('email')]);

        dd($shopInfo);

        return Redirect::route('summary.dashboard')->with('success', $login['message']);
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function register()
    {
        return view('users.register');
    }

    /**
     * @param RegisterRequest $request
     * @return mixed
     */
    public function registerHandle(RegisterRequest $request)
    {
        $register = $this->_userRepository->register($request->only(['email','password']));
        if( ! $register['status'])
        {
            return Redirect::route('users.register')->with('error', $register['message']);
        }

        return Redirect::route('summary.dashboard')->with('success', $register['message']);
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function forgotPassword()
    {
        return view('users.forgotPassword');
    }

    public function forgotPasswordHandle(ForgotPasswordRequest $request)
    {
        $email = $request->input('email');
        $this->_userRepository->forgotPassword($email);
    }

    /**
     * @return mixed
     */
    public function logout()
    {
        $logout = $this->_userRepository->logout();
        return $logout;
    }
}
