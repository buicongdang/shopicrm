<?php

namespace App\Http\Middleware;

use App\Http\Controllers\AppsController;
use App\Services\ShopifyServices;
use Closure;

class ShopifyCheck
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $req = $request->all();
        $shopifyService = new ShopifyServices();
        try{
            $shopifyService->getAccessToken(session('accessToken'), session('shopDomain'));
            return $next($request);
        } catch (\Exception $exception)
        {
            if(isset($req['shop']))
            {
                //Re auth
                $shopifyService->setShopDomain($req['shop']);
                $installUrl = $shopifyService->installURL();
                return redirect($installUrl);
            }
            return redirect(route('shopify.installApp'))->with('error', $exception->getMessage());
        }
    }
}
