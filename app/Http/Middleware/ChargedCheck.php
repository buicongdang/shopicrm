<?php

namespace App\Http\Middleware;

use App\Repository\ShopsRepository;
use App\ShopifyApi\ChargedApi;
use Closure;

class ChargedCheck
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $chargedApi = new ChargedApi();
        /**
         * @var ShopsRepository
         */
        $shopRepo = app(ShopsRepository::class);
        try{
            //Get shop info from database
            $shopInfo = $shopRepo->detail(['shop_id' => session('shopId')]);
            if($shopInfo['status'])
            {
                //Get charged info from api and check charge status "active"
                $chargedApi->getAccessToken(session('accessToken'), session('shopDomain'));
                $chargedInfo = $chargedApi->detailCharge($shopInfo['shopInfo']->billing_id);
                if($chargedInfo['status'])
                {
                    if($chargedInfo['detailCharge']->status == 'active')
                        return $next($request);
                }

                return redirect(route('apps.addCharged'));
            }

            return redirect(route('apps.errors404'));
        } catch (\Exception $exception)
        {
            return redirect(route('apps.addCharged'));
        }
    }
}
