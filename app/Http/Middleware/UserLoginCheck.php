<?php

namespace App\Http\Middleware;

use App\Repository\ShopsRepository;
use App\Repository\UsersRepository;
use Cartalyst\Sentinel\Native\Facades\Sentinel;
use Closure;
use Illuminate\Support\Facades\Lang;

class UserLoginCheck
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(Sentinel::check())
        {
            return $next;
        }

        return route('users.login');
    }
}
