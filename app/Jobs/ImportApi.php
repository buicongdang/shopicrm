<?php

namespace App\Jobs;

use App\Factory\RepositoryFactory;
use App\Factory\ShopifyApiFactory;
use App\Repository\ShopsRepository;
use App\ShopifyApi\CustomersApi;
use App\ShopifyApi\OrdersApi;
use App\ShopifyApi\ProductsApi;
use Dompdf\Exception;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class ImportApi implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

	private $_orderRepo;
	private $_customerRepo;
	private $_productRepo;
	private $_shopRepo;

	private $accessToken;
	private $shopDomain;

	private $_productsApi;
	private $_ordersApi;
	private $_customersApi;

	/**
	 * The number of times the job may be attempted.
	 *
	 * @var int
	 */
	public $tries = 3;

	/**
	 * The number of seconds the job can run before timing out.
	 *
	 * @var int
	 */
	public $timeout = 120;

	/**
	 * Create a new job instance.
	 *
	 * @param $accessToken
	 * @param $shopDomain
	 */
    public function __construct($accessToken, $shopDomain)
    {
    	$this->shopDomain = $shopDomain;
    	$this->accessToken = $accessToken;

	    $this->_orderRepo = RepositoryFactory::orderFactory();
	    $this->_customerRepo = RepositoryFactory::customersFactory();
	    $this->_productRepo = RepositoryFactory::productFactory();
	    $this->_shopRepo = RepositoryFactory::shopFactory();

	    $this->_ordersApi = ShopifyApiFactory::ordersFactory();
	    $this->_customersApi = ShopifyApiFactory::customersFactory();
	    $this->_productsApi = ShopifyApiFactory::productsFactory();
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
	    echo 'Start job import api ';

	    $import_orders = $this->importOrders();
	    if($import_orders){
	    	echo ' - Import orders Successful ';
	    } else{
		    echo ' - Import orders Fail ';
	    }


	    $import_products = $this->importProducts();
	    if($import_products){
		    echo '-  Import products Successful ';
	    } else{
		    echo '-  Import products Fail ';
	    }

	    $import_customers = $this->importCustomers();
	    if($import_customers){
		    echo ' - Import customers Successful ';
	    } else{
		    echo ' - Import customers Fail ';
	    }

	    echo ' - End job import api';

    }

    public function importOrders()
    {
	    try{
	    	$shop_info = $this->_shopRepo->detail(array('domain' => $this->shopDomain));
		    if(!empty($shop_info)){
			    $this->_ordersApi->getAccessToken($this->accessToken, $this->shopDomain);
			    $total_orders = $this->_ordersApi->count('any');
			    $fields = array(
				    // add where
			    );
			    if($total_orders['status']){
				    $total_page = ceil(intval($total_orders['count']) /  config('common.api_limit')) ;
				    if($total_page > 1){
					    for($i =1; $i <= $total_page; $i++){
						    $orders = $this->_ordersApi->all($fields,$i,config('common.api_limit'));
						    if($orders['status']){
							    foreach ($orders['orders'] as &$v){
								    $v->shop_id = $shop_info->id;
							    }
							    $save =  $this->_orderRepo->addMultiOrder($orders['orders']);
							    if(!$save) return false;
						    }else{
							    return false;
						    }
					    }
					    return true;
				    }

				    $orders = $this->_ordersApi->all($fields,1,config('common.api_limit'));
				    if($orders['status']){
				    	foreach ($orders['orders'] as &$v){
						    $v->shop_id = $shop_info->id;
					    }
					    $save =  $this->_orderRepo->addMultiOrder($orders['orders']);
					    if($save) return true;
				    }
			    }
		    }
		    return false;
	    } catch (\Exception $exception)
	    {
		    //var_dump($exception->getMessage());
		    return false;
	    }
    }

    public function importProducts(){
	    try{
		    $shop_info = $this->_shopRepo->detail(array('domain' => $this->shopDomain));
		    if(!empty($shop_info)){
			    $this->_productsApi->getAccessToken($this->accessToken, $this->shopDomain);
			    $total_products = $this->_productsApi->count();

			    if($total_products['status']){
				    $fields = array(
					    // add where
				    );

				    $total_page = ceil(intval($total_products['count']) /  config('common.api_limit')) ;

				    if($total_page > 1){
					    for($i =1; $i <= $total_page; $i++){
						    $products = $this->_productsApi->all($fields,$i,config('common.api_limit'));
						    if($products['status']){
							    foreach ($products['products'] as &$v){
								    $v->shop_id = $shop_info->id;
							    }
							    $save =  $this->_productRepo->addMultiProduct($products);
							    if(!$save) return false;
						    }
						    else return false;
					    }
					    return true;
				    }

				    $products = $this->_productsApi->all($fields,1,config('common.api_limit'));
				    if($products['status']){
					    foreach ($products['products'] as &$v){
						    $v->shop_id = $shop_info->id;
					    }
					    $save =  $this->_productRepo->addMultiProduct($products);
					    if($save) return true;
				    }
			    }
		    }

		    return false;
	    } catch (\Exception $exception)
	    {
		    //var_dump($exception->getMessage());
		    return false;
	    }
    }


	public function importCustomers(){
		try{
			$shop_info = $this->_shopRepo->detail(array('domain' => $this->shopDomain));
			if(!empty($shop_info)){
				$this->_customersApi->getAccessToken($this->accessToken, $this->shopDomain);
				$total_customers = $this->_customersApi->count();

				if($total_customers['status']){
					$fields = array(
						// add where
					);

					$total_page = ceil(intval($total_customers['count']) /  config('common.api_limit')) ;

					if($total_page > 1){
						for($i =1; $i <= $total_page; $i++){
							$customers = $this->_customersApi->all($fields,$i,config('common.api_limit'));
							if($customers['status']){
								foreach ($customers['customers'] as &$v){
									$v->shop_id = $shop_info->id;
								}
								$save =  $this->_customerRepo->addMultiCustomer($customers);
								if(!$save) return false;
							}
							else return false;
						}
						return true;
					}

					$customers = $this->_customersApi->all($fields,1,config('common.api_limit'));
					if($customers['status']){
						foreach ($customers['customers'] as &$v){
							$v->shop_id = $shop_info->id;
						}
						//var_dump($customers);
						$save =  $this->_customerRepo->addMultiCustomer($customers['customers']);
						if($save) return true;
					}
				}
			}

			return false;
		} catch (\Exception $exception)
		{
//			var_dump($exception->getMessage());
			return false;
		}
	}

	/**
	 * The job failed to process.
	 *
	 * @param  \Exception  $exception
	 * @return void
	 */
	public function failed(\Exception $exception)
	{
		// Send user notification of failure, etc...
	}

}
