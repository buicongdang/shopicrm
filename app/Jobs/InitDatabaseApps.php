<?php

namespace App\Jobs;

use App\Helpers\Helpers;
use App\Models\CommentsModel;
use App\Models\ShopMetaModel;
use App\Repository\CommentBackEndRepository;
use App\Repository\ShopMetaRepository;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class InitDatabaseApps implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    /**
     * @var \Illuminate\Foundation\Application|mixed
     */
    private $_shopMetaRepo;
    /**
     * @var \Illuminate\Foundation\Application|mixed
     */
    private $_commentBackendRepo;
    /**
     * @var
     */
    private $_shopId;

    private $_shopMetaModel;

    /**
     * InitDatabaseApps constructor.
     * @param $shopId
     */
    public function __construct($shopId)
    {
        $this->_shopId = $shopId;
        /**
         * @var ShopMetaRepository
         */
        $this->_shopMetaRepo =  app(ShopMetaRepository::class);
        /**
         * @var CommentsModel
         */
        $this->_commentBackendRepo = app(CommentBackEndRepository::class);
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $settingDefault = config('settings');
        $settingDefault['shop_id'] = $this->_shopId;
        //Create Shop Meta
        $this->_shopMetaRepo->create($settingDefault);

        //Create table comment_shop_id
        if( ! $this->_commentBackendRepo->createTable($this->_shopId))
            Helpers::saveLog('error', ['message' => 'Cannot create database', 'file' => __FILE__, 'line' => __LINE__, 'function' => __FUNCTION__]);
    }
}
