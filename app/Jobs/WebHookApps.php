<?php

namespace App\Jobs;

use App\Helpers\Helpers;
use App\ShopifyApi\WebHookApi;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;


class WebHookApps implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * @var WebHookApi
     */
    private $_webHookApi;

    /**
     * @var
     */
    private $_accessToken;

    /**
     * @var
     */
    private $_shopDomain;

    /**
     * WebHookApps constructor.
     * @param $accessToken
     * @param $shopDomain
     */
    public function __construct($accessToken, $shopDomain)
    {
        $this->_webHookApi = app(WebHookApi::class);
        $this->_accessToken = $accessToken;
        $this->_shopDomain = $shopDomain;
    }

    /**
     * @return array
     */
    private function listWebHook()
    {
        return [
            [
                'address' => route('webhook.uninstall_app'),
                'topic'   => 'app/uninstalled'
            ],
            [
                'address' => route('webhook.delete_product'),
                'topic'   => 'products/delete'
            ]
        ];
    }


    /**
     * Execute the job.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->_webHookApi->getAccessToken($this->_accessToken, $this->_shopDomain);
        //Delete all webHook before add Web Hook
        if( ! $this->deleteAllWebHook())
            return false;

        foreach ($this->listWebHook() as $k => $v)
        {
            $webHook = $this->_webHookApi->addWebHook($v['address'], $v['topic']);
            if( ! $webHook['status'])
            {
                Helpers::saveLog('error', ['message' => $webHook['message'], 'line' => __LINE__, 'file' => __FILE__, 'function' => __FUNCTION__, 'domain' => $this->_shopDomain]);
            }
        }
    }

    /**
     * @return bool
     */
    public function deleteAllWebHook()
    {
        $webHook = $this->_webHookApi->allWebHook();
        if( ! $webHook['status'])
        {
            Helpers::saveLog('error', ['message' => $webHook['message'], 'line' => __LINE__, 'file' => __FILE__, 'function' => __FUNCTION__, 'domain' => $this->_shopDomain]);
            return false;
        }

        foreach ($webHook['webHook'] as $k => $v)
        {
            $isDelete = $this->_webHookApi->delete($v->id);
            if( ! $isDelete['status'])
                Helpers::saveLog('error', ['message' => $isDelete['message'], 'line' => __LINE__, 'function' => __FUNCTION__, 'domain' => $this->_shopDomain]);

        }

        return true;
    }

}
