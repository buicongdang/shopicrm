<?php

namespace App\Helpers;

/**
 * Class Helpers
 * @package App\Helpers
 */
class Helpers
{
    /**
     * @param $name
     * @param $data
     * @return string
     */
    public static function parseTojson ( $name, $data )
    {
        $record = array();
        foreach ($data as $key => $value) {
            if (!empty($value))
                $record[$key][$name] = $value;
        }
        return json_encode($record);
    }

    /**
     * @param $currentPage
     * @param $totalItem
     * @param $itemInPage
     * @param $filterPagination
     * @return string
     */
    public static function paginationApi($currentPage, $totalItem, $itemInPage, $filterPagination)
    {
        $totalPage = ceil($totalItem/$itemInPage);
        $view = view('sections.paginations', compact('currentPage', 'totalPage', 'filterPagination'))->render();
        return $view;
    }

    /**
     * @param $type
     * @param $arrayLog
     */
    public  static function saveLog($type, $arrayLog)
    {
        $message = isset($arrayLog['message']) ? $arrayLog['message'] : '';
        $file = isset($arrayLog['file']) ? ' File: '.$arrayLog['file'] : '';
        $line = isset($arrayLog['line']) ? ' Line: '.$arrayLog['line'] : '';
        $function = isset($arrayLog['function']) ? ' Function: '.$arrayLog['function'] : '';
        $domain =  isset($arrayLog['domain']) ? ' Domain: '.$arrayLog['domain'] : '';
        Log::$type($message.$file.$line.$function.$domain);
    }
}