<?php
return [
	'failed' => 'An error, please try again.',
	'create_success' => 'Save membership successful.',
	'update_success' => 'Update membership successful.',
	'delete_success' => 'Delete membership successful.',
];