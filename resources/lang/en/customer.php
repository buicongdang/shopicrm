<?php
return [
    'failed' => 'An error, please try again.',
    'create_success' => 'Customer created successfully.',
    'update_success' => 'Update membership successful.',
    'delete_success' => 'Delete membership successful.',
    'table_header_name' => 'Name',
    'email' => 'Email',
    'phone' => 'Phone',
    'status' => 'Status',
    'action' => 'Action',
];