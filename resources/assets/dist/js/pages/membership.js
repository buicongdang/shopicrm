var $ = jQuery.noConflict();
$(document).ready(function () {
    $('#datepicker').datepicker({
        autoclose: true
    });

    if ($('#featured_image').length)
        el("featured_image").addEventListener("change", readImage, false);

    /**
     * Delete membership
     */
    $('.btn-delete-membership').click(function (e) {
        e.preventDefault();
        var modal = '#modalDeleteMembership';
        var id = $(this).attr('data-id');

        $(modal).on('shown.bs.modal', function () {
            var obj = $(this);
            $('input[name="id"]', obj).val(id);
        });

        var originalModal = $(modal).clone();
        $(modal).on('hidden.bs.modal', function () {
            $(modal).remove();
            var myClone = originalModal.clone();
            $('body').append(myClone);
        });

        $(modal).modal({
            show: 'true'
        });
    });

    $('body').delegate('.form-delete-membership', 'submit', function (e) {
        e.preventDefault();
        var obj = $(this);
        var id = $('input[name="id"]', obj).val();
        var ajax_url = obj.attr('action');

        $.ajax({
            type: "post",
            url: ajax_url,
            dataType: 'json',
            data: obj.serialize(),
            beforeSend: function () {
                $('input, button, textarea', obj).attr('disabled', true).css('opacity', 0.5);
            },
            success: function (data, statusText, xhr) {
                $('input, button, textarea', obj).attr('disabled', false).css('opacity', 1);
                if (data.status) {
                    alert(data.message);
                    $('#tr-membership-' + id).remove();
                    $('#modalDeleteMembership').modal('hide');
                } else {
                    alert(data.message);
                }
            }
        });
    });
});

function el(id) {
    return document.getElementById(id);
} // Get elem by ID
function readImage() {
    if (this.files && this.files[0]) {
        var FR = new FileReader();
        FR.onload = function (e) {
            el("featured_image_result").src = e.target.result;
        };
        FR.readAsDataURL(this.files[0]);
    }
}