@extends('layout.backend')
@section('container_content')
        <h1>Sign Up</h1>
        @foreach(config('stripe.plan') as $k=>$v)
            <option  value="{{ $v }}">{{  $k }}</option>
        @endforeach
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-md-3">
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <h3 class="panel-title">
                                Day</h3>
                        </div>
                        <div class="panel-body">
                            <div class="the-price">
                                <h1>
                                    $10<span class="subscript">/Daily</span></h1>
                                <small>Manager 10 User/1 day</small>
                            </div>
                        </div>
                        <div class="panel-footer">
                            <form action="/stripe" method="POST">
                                <script
                                        src="https://checkout.stripe.com/checkout.js" class="stripe-button"
                                        data-key="pk_test_OhCQ6G2VwJMtEb2NFoLkxpn7"
                                        data-amount="999"
                                        data-name="Demo Site"
                                        data-description="Widget"
                                        data-image="https://stripe.com/img/documentation/checkout/marketplace.png"
                                        data-locale="auto">
                                </script>
                                <input type="hidden" name="_token" id="_token" value="{{ csrf_token() }}">
                                <input type="hidden" name="plan"  value="day">
                            </form>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-md-3">
                    <div class="panel panel-success">
                        <div class="panel-heading">
                            <h3 class="panel-title">
                                Basic</h3>
                        </div>
                        <div class="panel-body">
                            <div class="the-price">
                                <h1>
                                    $20<span class="subscript">/mo</span></h1>
                                <small>Manager 100 User/1 month</small>
                            </div>
                        </div>
                        <div class="panel-footer">
                            <form action="/stripe" method="POST">
                                <script
                                        src="https://checkout.stripe.com/checkout.js" class="stripe-button"
                                        data-key="pk_test_OhCQ6G2VwJMtEb2NFoLkxpn7"
                                        data-amount="999"
                                        data-name="Demo Site"
                                        data-description="Widget"
                                        data-image="https://stripe.com/img/documentation/checkout/marketplace.png"
                                        data-locale="auto">
                                </script>
                                <input type="hidden" name="_token" id="_token" value="{{ csrf_token() }}">
                                <input type="hidden" name="plan"  value="basic">
                            </form>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-md-3">
                    <div class="panel panel-info">
                        <div class="panel-heading">
                            <h3 class="panel-title">
                                Pro</h3>
                        </div>
                        <div class="panel-body">
                            <div class="the-price">
                                <h1>
                                    $35<span class="subscript">/mo</span></h1>
                                <small>Manager 400 User/1 month</small>
                            </div>
                        </div>
                        <div class="panel-footer">
                            <form action="/stripe" method="POST">
                                <script
                                        src="https://checkout.stripe.com/checkout.js" class="stripe-button"
                                        data-key="pk_test_OhCQ6G2VwJMtEb2NFoLkxpn7"
                                        data-name="Demo Site"
                                        data-description="Widget"
                                        data-image="https://stripe.com/img/documentation/checkout/marketplace.png"
                                        data-locale="auto">
                                </script>
                                <input type="hidden" name="_token" id="_token" value="{{ csrf_token() }}">
                                <input type="hidden" name="plan"  value="pro">
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
@endsection