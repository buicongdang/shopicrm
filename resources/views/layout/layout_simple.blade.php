<!DOCTYPE html>
<html>
<head>
    @include('layout.header')
</head>
<body>
<div class="container">
    <div class="col-sm-10 col-sm-offset-1">
        @yield('container_content')
    </div>
    @include('layout.footer')
</div>

{{--Extend scripts--}}
@yield('scripts')
</body>
</html>
