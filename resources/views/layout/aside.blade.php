<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar user panel -->
        {{--<div class="user-panel">--}}
            {{--<div class="pull-left image">--}}
                {{--<img src="{{ URL::asset('images/user2-160x160.jpg') }}" class="img-circle" alt="User Image">--}}
            {{--</div>--}}
            {{--<div class="pull-left info">--}}
                {{--<p>Alexander Pierce</p>--}}
                {{--<a href="#"><i class="fa fa-circle text-success"></i> Online</a>--}}
            {{--</div>--}}
        {{--</div>--}}

        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu" data-widget="tree">
            <li class="active">
                <a href="{{ route('summary.dashboard') }}">
                    <i class="fa fa-dashboard"></i> <span>Dashboard</span>
                </a>
            </li>
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-table"></i> <span>Customers</span>
                    <span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="{{ route('customer.list') }}"><i class="fa fa-circle-o"></i> List</a></li>
                    <li><a href="{{ route('customer.created') }}"><i class="fa fa-circle-o"></i> Add new</a></li>
                </ul>
            </li>
            <li class="treeview">
                <a href="{{ route('membership.list') }}">
                    <i class="fa fa-table"></i> <span>Membership</span>
                    <span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="{{ route('membership.list') }}"><i class="fa fa-circle-o"></i> List</a></li>
                    <li><a href="{{ route('membership.create') }}"><i class="fa fa-circle-o"></i> Add new</a></li>
                </ul>
            </li>
            <li class="treeview">
                <a href="{{ route('campaigns_auto.home') }}">
                    <i class="fa fa-table"></i> <span>Campaigns</span>
                    <span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span>
                    <ul class="treeview-menu">
                        <li><a href="{{ route('campaigns_auto.home') }}"><i class="fa fa-circle-o"></i> Add new</a></li>
                    </ul>
                </a>
            </li>
            <li>
                <a href="#">
                    <i class="fa fa-book"></i>
                    <span>Documentation</span>
                </a>
            </li>
        </ul>
    </section>
    <!-- /.sidebar -->
</aside>