<!DOCTYPE html>
<html>
<head>
    @include('layout.header')
    {{--extend style--}}
    @yield('styles')
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
    @include('sections.localizationjs')
    @include('layout.navi')
    @include('layout.aside')

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        @include('layout.breadcrumb')

        <!-- Main content -->
        <section class="content">
            @yield('container_content')
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
    <footer class="main-footer">
        <div class="pull-right hidden-xs">
            <b>Version</b> 2.4.0
        </div>
        <strong>
            Copyright &copy; 2014-2016 <a href="https://adminlte.io">Almsaeed Studio</a>.
        </strong>
        All rights reserved.
    </footer>
    <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->
@include('layout.footer')

{{--Extend scripts--}}
@yield('scripts')

</body>
</html>
