@extends('layout.backend')

@section('container_content')
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">List Membership</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body table-responsive no-padding">
                    <table class="table table-hover">
                        <tr>
                            <th>STT</th>
                            <th>Name</th>
                            <th>Description</th>
                            <th>Order Price</th>
                            <th>Action</th>
                        </tr>

                        @if(!empty($list->total()))
                            @foreach ($list as $key => $v)
                                <tr id="tr-membership-{{ $v->id }}">
                                    <td>{{ $key + 1 }}</td>
                                    <td>
                                        {{ $v->name }}
                                    </td>
                                    <td>
                                        {{ $v->description }}
                                    </td>
                                    <td>
                                        {{ $v->order_price }}
                                    </td>
                                    <td style="white-space: nowrap">
                                        <a href="{{ route( 'membership.update',['id' => $v->id] ) }}" title="Edit">
                                            <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                                        </a>
                                        |
                                        <a href="javascript:void(0)" data-id="{{ $v->id }}"
                                           class="btn-delete-membership" title="Trash">
                                            <i class="fa fa-trash" aria-hidden="true"></i>
                                        </a>
                                    </td>
                                </tr>
                            @endforeach
                        @else
                            <tr>
                                <td colspan="5">No results</td>
                            </tr>
                        @endif
                    </table>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="modalDeleteMembership" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Delete membership</h4>
                </div>
                <div class="modal-body">
                    <p>Do you want delete this membership?</p>
                </div>
                <div class="modal-footer">
                    <form class="form-delete-membership" action="{{ route('membership.delete') }}">
                        <input type="hidden" name="id" value="">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">

                        <button type="submit" class="btn btn-primary">Delete</button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection


@section('scripts')
    <script type="text/javascript" src="{{ mix('dist/js/pages/membership.min.js')}}"></script>
@endsection