@extends('layout.backend')


@section('container_content')
    @include('sections.alert_error')
    @include('sections.alert_success')

    <div class="box box-primary">
        <form role="form" method="post" enctype="multipart/form-data">
            <div class="box-body">
                <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                    <label for="membership_name">Name *</label>
                    <input type="text" class="form-control" name="name" id="membership_name" placeholder="Enter name"
                           value="{{ old('name')  }}">

                    @if ($errors->has('name'))
                        <span class="help-block">
                            <strong>{{ $errors->first('name') }}</strong>
                        </span>
                    @endif
                </div>

                <div class="form-group{{ $errors->has('order_price') ? ' has-error' : '' }}">
                    <label for="order_price">Order price *</label>
                    <input type="text" class="form-control" name="order_price" id="order_price"
                           placeholder="Enter name" value="{{ old('order_price') }}">

                    @if ($errors->has('order_price'))
                        <span class="help-block">
                            <strong>{{ $errors->first('order_price') }}</strong>
                        </span>
                    @endif
                </div>

                <div class="form-group">
                    <label for="membership_description">Description</label>
                    <textarea rows="5" name="description" id="membership_description"
                              class="form-control">{{ old('description') }}</textarea>
                </div>


                <div class="form-group{{ $errors->has('image') ? ' has-error' : '' }}">
                    <div class="row">
                        <div class="col-md-4">
                            <label for="featured_image" class="control-label">Image *</label>
                            <input type="file" name="image" id="featured_image"/>
                        </div>
                        <div class="col-md-8">
                            <img id="featured_image_result" src="" class="img-rounded" style="max-width: 100%;width: 200px"/>
                        </div>
                    </div>


                    @if ($errors->has('image'))
                        <span class="help-block">
                            <strong>{{ $errors->first('image') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <input type="hidden" name="_token" id="_token" value="{{ csrf_token() }}">
            <div class="box-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
            </div>
        </form>
    </div>
@endsection

@section('scripts')
    <script type="text/javascript" src="{{ mix('dist/js/pages/membership.min.js')}}"></script>
@endsection