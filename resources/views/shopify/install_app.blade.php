@extends('layout.layout_simple')

@section('container_content')
    <div class="clearfix">
        <div class="col-sm-8 col-sm-offset-2">
            <div class="body-logo text-center">
                <a href="#"><h1>Admin LTE</h1></a>
            </div>
            @include('sections.alert_error')
            @include('sections.alert_success')
            <form method="post" action="{{ route('shopify.installAppHandle') }}">
                {{ csrf_field() }}
                <div class="form-group {{ $errors->has('shopDomain') ? 'has-error' : '' }}">
                    <div class="input-group input-group-lg">
                        <input type="text" class="form-control" value="{{ old('shopDomain') }}" placeholder="Ex: alireviews.myshopify.com" name="shopDomain" />
                        <span class="input-group-btn">
                          <button type="submit" class="btn btn-warning">Install App</button>
                        </span>
                    </div>
                    <span class="help-block">{{ $errors->first('shopDomain') }}</span>
                </div>
            </form>
        </div>

    </div>
@endsection