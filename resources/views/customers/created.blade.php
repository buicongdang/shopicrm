@extends('layout.backend')
@section('container_content')
    <div class="row">
        <!-- left column -->
        <div class="col-md-12">
            <div class="box box-primary">
                @include('sections.alert_error')
                @include('sections.alert_success')
                <form role="form" method="post" action="{{ route('customer.createHandle') }}"
                      enctype="multipart/form-data">
                    <div class="box-body">
                        <div class="col-md-6 form-group">
                            <div class="form-group col-xs-6">
                                <label for="first_name">First Name *</label>
                                <input type="text" class="form-control" name="first_name" id="first_name"
                                       placeholder="Enter name"
                                       value="{{ old('first_name') }}"
                                       tabindex="1">
                                <span class="error error-first-name">{{ $errors->first('first_name') }}</span>
                            </div>
                            <div class="form-group col-xs-6">
                                <label for="last_name">Last Name *</label>
                                <input type="text" class="form-control" name="last_name" id="last_name"
                                       placeholder="Enter name"
                                       value="{{ old('last_name') }}" tabindex="2">
                                <span class="error error-first-name">{{ $errors->first('last_name') }}</span>
                            </div>
                            <div class="form-group col-xs-6">
                                <label for="email_primary">Email Primary *</label>
                                <div class="input-group">
                                    <div class="input-group-addon"><i class="fa fa-envelope"></i></div>
                                    <input type="text" name="email_primary" id="email_primary" class="form-control"
                                           tabindex="3"
                                           value="{{ old('email_primary')}}">
                                </div>
                                <span class="error error-email-primary">{{ $errors->first('email_primary') }}</span>
                            </div>
                            <div class="form-group col-xs-6">
                                <label for="phone_primary">Phone Primary *</label>
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <i class="fa fa-phone"></i>
                                    </div>
                                    <input type="text" class="form-control" name="phone_primary" id="phone_primary"
                                           placeholder="Enter name"
                                           value="{{ old('phone_primary') }}"
                                           tabindex="5">
                                </div>
                                <span class="error error-first-name">{{ $errors->first('phone_primary') }}</span>
                            </div>
                            <div class="form-group col-xs-8" id="p_emails">
                                <label for="emails">Add more email
                                    <button class="btn btn-primary btn-xs text-center addScnt"><span
                                                class="fa fa-plus"></span></button>
                                </label>
                                <div class="input-group">
                                    <div class="input-group-addon"><i class="fa fa-envelope"></i></div>
                                    <input type="text" class="form-control" name="emails[]" placeholder="list email"
                                           value="" tabindex="4">
                                </div>
                                <span class="error error-first-name">{{ $errors->first('emails') }}</span>
                            </div>
                            <div class="form-group col-xs-8" id="p_phones">
                                <label for="phones">Add more email
                                    <button class="btn btn-primary btn-xs text-center addScnt"><span
                                                class="fa fa-plus"></span></button>
                                </label>
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <i class="fa fa-phone"></i>
                                    </div>
                                    <input type="text" class="form-control" name="phones[]" id="phones"
                                           placeholder="List Phone" value="" tabindex="6">
                                </div>
                                <span class="error error-first-name">{{ $errors->first('phones') }}</span>
                            </div>
                            <div class="form-group col-xs-6">
                                <label for="featured_image" class="control-label">Avatar *</label>
                                <input type="file" name="avatar" id="featured_image">
                                <span class="error error-first-name">{{ $errors->first('avatar') }}</span>
                            </div>
                        </div>
                        <div class="col-md-6 form-group">
                            <div class="form-group col-xs-6">
                                <label>Date of Birth *</label>
                                <div class="input-group date">
                                    <div class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </div>
                                    <input type="text" class="form-control pull-right" id="datepicker" name="birth_day"
                                           tabindex="7"
                                           value="{{ old('birth_day') }}">
                                </div>
                                <span class="error error-first-name">{{ $errors->first('birth_day') }}</span>
                            </div>
                            <div class="form-group col-xs-6">
                                <label>Member Level *</label>
                                <select class="form-control" name="member_level">
                                    @if(!empty($listMember))
                                        @foreach($listMember as $item)
                                            <option {{ ( old('member_level') == $item->id) ? 'selected': ''}} value="{{$item->id}}">{{$item->name}}</option>
                                        @endforeach
                                    @endif
                                </select>
                                <span class="error error-member-level">{{ $errors->first('member_level') }}</span>
                            </div>
                            <div class="form-group col-xs-12">
                                <label for="address">Address *</label>
                                <input type="text" class="form-control" name="address" id="address"
                                       placeholder="address"
                                       value="{{ old('address') }}" tabindex="8">
                                <span class="error error-first-name">{{ $errors->first('address') }}</span>
                            </div>
                            <div class="form-group col-xs-6">
                                <label for="province">Province</label>
                                <input type="text" class="form-control" name="province" id="province"
                                       placeholder="province"
                                       value="{{ old('province') }}" tabindex="9">
                            </div>

                            <div class="form-group col-xs-6">
                                <label for="city">City</label>
                                <input type="text" class="form-control" name="city" id="city"
                                       placeholder="city" value="{{ old('city') }}"
                                       tabindex="10">
                            </div>

                            <div class="form-group col-xs-6">
                                <label for="country">Country</label>
                                <input type="text" class="form-control" name="country" id="country"
                                       placeholder="country"
                                       value="{{ old('country') }}" tabindex="11">
                            </div>

                            <div class="form-group col-xs-6">
                                <label for="country_code">Country Code</label>
                                <input type="text" class="form-control" name="country_code" id="country_code"
                                       placeholder="country_code"
                                       value="{{ old('country_code') }}"
                                       tabindex="12">
                            </div>
                        </div>
                    </div>
                    <div class="box-footer">
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </div>
                    <input type="hidden" name="_token" id="_token" value="{{ csrf_token() }}">
                </form>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    <script type="text/javascript" id="fdsfds" src="{{ asset('assets/js/customer.js')}}"></script>
@endsection
@section('styles')
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/customer.css')}}">
@endsection

