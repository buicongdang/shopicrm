@extends('layout.backend')
@section('container_content')
    <div class="row">
        <!-- left column -->
        <div class="col-md-12">
            <div class="box box-primary">
                @include('sections.alert_error')
                @include('sections.alert_success')
                <form role="form" method="post" action="{{ route('customer.updateHandle') }}"
                      enctype="multipart/form-data">
                    <div class="box-body">
                        <div class="col-md-6 form-group">
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-5">
                                        <label for="featured_image" class="control-label">Avatar *</label>
                                        <input type="file" name="avatar" id="featured_image">
                                    </div>
                                    <div class="col-md-7">
                                        <img id="featured_image_result"
                                             src="{{ (isset( $customer->avatar ) ? asset('storage/avatar/' . $customer->avatar) : "") }}"
                                             class="img-rounded" style="max-width: 100%;width: 200px"/>
                                    </div>
                                </div>
                                <span class="error error-first-name">{{ $errors->first('avatar') }}</span>
                            </div>
                            <div class="form-group col-xs-6">
                                <label for="phone_primary">Phone Primary *</label>
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <i class="fa fa-phone"></i>
                                    </div>
                                    <input type="text" class="form-control" name="phone_primary" id="phone_primary"
                                           placeholder="Enter name"
                                           value="{{ (isset( $customer->phone_primary ) ? $customer->phone_primary : "") }}"
                                           tabindex="5">
                                </div>
                                <span class="error error-first-name">{{ $errors->first('phone_primary') }}</span>
                            </div>
                            <div class="form-group col-xs-6">
                                <label for="email_primary">Email Primary</label>
                                <div class="input-group">
                                    <div class="input-group-addon"><i class="fa fa-envelope"></i></div>
                                    <input type="text" name="email_primary" id="email_primary" class="form-control"
                                           tabindex="3"
                                           value="{{ (isset( $customer->email_primary ) ? $customer->email_primary : "") }}">
                                </div>
                                <span class="error error-email-primary">{{ $errors->first('email_primary') }}</span>
                            </div>
                            <div class="form-group col-xs-6" id="p_phones">
                                <label for="phones">Add more email <button class="btn btn-primary btn-xs text-center addScnt"><span class="fa fa-plus"></span></button></label>
                                @foreach (json_decode($customer->phones) as $phone)
                                    <div class="input-group phones">
                                        <div class="input-group-addon">
                                            <i class="fa fa-phone"></i>
                                        </div>
                                        <input type="text" class="form-control" name="phones[]" id="phones"
                                               placeholder="List Phone"
                                               value="{{ (isset( $phone->phones ) ? $phone->phones : "") }}"
                                               tabindex="6">
                                        <span class="input-group-btn"><button type="button" data-type="phones" class="btn btn-info btn-flat remScnt"><i class="fa fa-trash-o"></i></button></span>
                                    </div>
                                @endforeach
                                <span class="error error-first-name">{{ $errors->first('phones') }}</span>
                            </div>
                            <div class="form-group col-xs-6" id="p_emails">
                                <label for="emails">Add more email <button class="btn btn-primary btn-xs text-center addScnt"><span class="fa fa-plus"></span></button></label>
                                @foreach(json_decode($customer->emails) as $email)
                                    <div class="input-group emails">
                                        <div class="input-group-addon"><i class="fa fa-envelope"></i></div>
                                        <input type="text" class="form-control" name="emails[]" id="emails"
                                               placeholder="list email"
                                               value="{{ (isset( $email->emails ) ? $email->emails : "") }}"
                                               tabindex="4">
                                        <span class="input-group-btn"><button type="button" data-type="emails" class="btn btn-info btn-flat remScnt"><i class="fa fa-trash-o"></i></button></span>
                                    </div>
                                @endforeach
                                <span class="error error-first-name">{{ $errors->first('emails') }}</span>
                            </div>
                        </div>
                        <div class="col-md-6 form-group">
                            <div class="form-group col-xs-6">
                                <label for="first_name">First Name *</label>
                                <input type="text" class="form-control" name="first_name" id="first_name"
                                       placeholder="Enter name"
                                       value="{{ (isset( $customer->first_name ) ? $customer->first_name : "") }}"
                                       tabindex="1">
                                <span class="error error-first-name">{{ $errors->first('first_name') }}</span>
                            </div>
                            <div class="form-group col-xs-6">
                                <label for="last_name">Last Name *</label>
                                <input type="text" class="form-control" name="last_name" id="last_name"
                                       placeholder="Enter name"
                                       value="{{ (isset( $customer->last_name ) ? $customer->last_name : "") }}"
                                       tabindex="2">
                                <span class="error error-first-name">{{ $errors->first('last_name') }}</span>
                            </div>
                            <div class="form-group col-xs-6">
                                <label for="status">Status *</label>
                                <select name="status" class="form-control">
                                    @foreach(array_reverse(config('common.status')) as $k=>$v)
                                        <option {{ (isset($_POST['status']) && $_POST['status'] == $v) ? 'selected' : ((isset($customer->status) && $customer->status == $v) ? 'selected' : '') }} value="{{ $v }}">{{  $k }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group col-xs-6">
                                <label>Member Level *</label>
                                <select class="form-control" name="member_level">
                                    @foreach($listMember as $item)
                                        <option {{ ((isset($customer->member_level) && $customer->member_level == $item->id) ? 'selected' : '') }} value="{{$item->id}}">{{$item->name}}</option>
                                    @endforeach
                                </select>
                                <span class="error error-member-level">{{ $errors->first('member_level') }}</span>
                            </div>
                            <div class="form-group col-xs-6">
                                <label for="address">Address *</label>
                                <input type="text" class="form-control" name="address" id="address"
                                       placeholder="address" tabindex="8"
                                       value="{{ (isset( $customer->address ) ? $customer->address : "") }}">
                                <span class="error error-first-name">{{ $errors->first('address') }}</span>
                            </div>
                            <div class="form-group col-xs-6">
                                <label>Date of Birth *</label>
                                <div class="input-group date">
                                    <div class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </div>
                                    <input type="text" class="form-control pull-right" id="datepicker" name="birth_day"
                                           tabindex="7"
                                           value="{{ (isset( $customer->birth_day ) ? $customer->birth_day : "") }}">
                                </div>
                                <span class="error error-first-name">{{ $errors->first('birth_day') }}</span>
                            </div>
                            <div class="form-group col-xs-6">
                                <label for="province">Province</label>
                                <input type="text" class="form-control" name="province" id="province"
                                       placeholder="province"
                                       value="{{ (isset( $customer->province ) ? $customer->province : "") }}"
                                       tabindex="9">
                            </div>

                            <div class="form-group col-xs-6">
                                <label for="city">City</label>
                                <input type="text" class="form-control" name="city" id="city"
                                       placeholder="city"
                                       value="{{ (isset( $customer->city ) ? $customer->city : "") }}" tabindex="10">
                            </div>

                            <div class="form-group col-xs-6">
                                <label for="country">Country</label>
                                <input type="text" class="form-control" name="country" id="country"
                                       placeholder="country"
                                       value="{{ (isset( $customer->country ) ? $customer->country : "") }}"
                                       tabindex="11">
                            </div>

                            <div class="form-group col-xs-6">
                                <label for="country_code">Country Code</label>
                                <input type="text" class="form-control" name="country_code" id="country_code"
                                       placeholder="country_code"
                                       value="{{ (isset( $customer->country_code ) ? $customer->country_code : "") }}"
                                       tabindex="12">
                            </div>
                        </div>
                        <span class="clearfix"></span>
                    </div>
                    <div class="box-footer">
                        <button type="submit" class="btn btn-primary">Update</button>
                    </div>
                    <input type="hidden" name="_token" id="_token" value="{{ csrf_token() }}">
                    <input type="hidden" name="id" id="id" value="{{ $customer->id }}">
                </form>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    <script type="text/javascript" id="fdsfds" src="{{ asset('assets/js/customer.js')}}"></script>
@endsection
@section('styles')
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/customer.css')}}">
@endsection

