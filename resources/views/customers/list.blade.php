@extends('layout.backend')

@section('container_content')
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">List Customer</h3>

                    <div class="box-tools">
                        <div class="input-group input-group-sm" style="width: 150px;">
                            <input type="text" name="table_search" class="form-control pull-right" placeholder="Search">

                            <div class="input-group-btn">
                                <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body table-responsive no-padding">
                    <table class="table table-hover">
                        <tr>
                            <th>ID</th>
                            <th>@sortablelink('last_name', __('customer.table_header_name'))</th>
                            <th>@sortablelink('email_primary', __('customer.email'))</th>
                            <th>@sortablelink('phone_primary', __('customer.phone'))</th>
                            <th>@sortablelink('status', __('customer.status'))</th>
                            <th>Action</th>
                        </tr>
                        @foreach($listCustomer as $item)
                            <tr>
                                <td>{{$item->id}}</td>
                                <td>{{$item->last_name}}</td>
                                <td>{{$item->email_primary}}</td>
                                <td>{{$item->phone_primary}}</td>
                                <td><span class="{{ $item->status ? "label label-success" :"label label-danger" }}">{{ array_search($item->status , config('common.status')) }}</span></td>
                                <td style="white-space: nowrap">
                                    <a href="{{ route('customer.update',['id' => $item->id ]) }}" title="Edit">
                                        <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                                    </a>
                                    |
                                    <a href="javascript:void(0)" data-id="" class="btn-delete-post" title="Trash">
                                        <i class="fa fa-trash" aria-hidden="true"></i>
                                    </a>
                                </td>
                            </tr>
                        @endforeach
                    </table>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
    </div>
@endsection