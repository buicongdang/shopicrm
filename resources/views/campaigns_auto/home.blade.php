@extends('layout.backend')

@section('container_content')
    <form action="{{ route('campaigns_auto.stepOne') }}" method="post">
        <div class="list clearfix">
            <h1>@lang('campaigns_auto.header_title_general')</h1>
            <div class="clearfix">
                <div class="campaigns_item col-lg-3 col-xs-6" name="type_campaigns" value="general.welcome">
                    <!-- small box -->
                    <div class="small-box bg-aqua">
                        <div class="inner">
                            <h3>150</h3>

                            <p>New Orders</p>
                        </div>
                        <div class="icon">
                            <i class="fa fa-shopping-cart"></i>
                        </div>
                        <a href="{{ route('campaigns_auto.stepOne',['type'=> 'general']) }}" class="small-box-footer">
                           Add New <i class="fa fa-arrow-circle-right"></i>
                        </a>
                    </div>
                </div>
            </div>

        </div>

        <div class="list clearfix">
            <h1>@lang('campaigns_auto.header_title_membership')</h1>
            <ul>
                @foreach($memberShips as $memberShip)
                    <li class="campaigns_item" style="border: 1px solid #cccccc; padding: 20px 5px;" >
                        <a href="{{ route('campaigns_auto.stepOne',['type'=> 'memberShip','id'=> $memberShip->id]) }}" class="small-box-footer">
                            Add New <i class="fa fa-arrow-circle-right"></i>
                        </a>{{ $memberShip->name }}
                    </li>
                @endforeach
            </ul>
        </div>

        <div class="list clearfix">
            <h1>@lang('campaigns_auto.header_title_before_buyer')</h1>
            <ul>
                <li class="plus">
                    <span class="glyphicon glyphicon-plus"></span>
                    <a href="{{ route('campaigns_auto.stepOne',['type'=> 'custom']) }}" class="small-box-footer">
                        Add New <i class="fa fa-arrow-circle-right"></i>
                    </a>
                </li>
            </ul>
        </div>
    </form>

@endsection

@section('styles')

@endsection

@section('scripts')
    <script type="text/javascript">
        $('.campaigns_item').click(function (e) {
            console.log(123);
        });
    </script>
@endsection