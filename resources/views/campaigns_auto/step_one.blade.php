@extends('layout.backend')
@section('container_content')
    <div class="row">
        <section>
            <div class="wizard">
                <div class="wizard-inner">
                    <div class="connecting-line"></div>
                    <ul class="nav nav-tabs" role="tablist">

                        <li role="presentation" class="active">
                            <a href="#step1" data-toggle="tab" aria-controls="step1" role="tab" title="Step 1">
                            <span class="round-tab">
                                <i class="glyphicon glyphicon-folder-open"></i>
                            </span>
                            </a>
                        </li>

                        <li role="presentation" class="disabled">
                            <a href="#step2" data-toggle="tab" aria-controls="step2" role="tab" title="Step 2">
                            <span class="round-tab">
                                <i class="glyphicon glyphicon-pencil"></i>
                            </span>
                            </a>
                        </li>
                        {{--<li role="presentation" class="disabled">
                            <a href="#step3" data-toggle="tab" aria-controls="step3" role="tab" title="Step 3">
                            <span class="round-tab">
                                <i class="glyphicon glyphicon-picture"></i>
                            </span>
                            </a>
                        </li>--}}

                        <li role="presentation" class="disabled">
                            <a href="#complete" data-toggle="tab" aria-controls="complete" role="tab" title="Complete">
                            <span class="round-tab">
                                <i class="glyphicon glyphicon-ok"></i>
                            </span>
                            </a>
                        </li>
                    </ul>
                </div>
                <form class="form-horizontal" method="post" action="{{ route('campaigns_auto.createHandle') }}">
                    <div class="tab-content">
                        <div class="tab-pane active" role="tabpanel" id="step1">
                            <div class="col-md-6">
                                <div class="box-body">
                                    <div class="form-group">
                                        <label for="title" class="col-sm-2 control-label">Title</label>

                                        <div class="col-sm-10">
                                            <input type="text" class="form-control" name="title" value="" id="title"
                                                   placeholder="Title">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="email_from" class="col-sm-2 control-label">Email</label>

                                        <div class="col-sm-10">
                                            <input type="text" class="form-control" name="email_from"
                                                   value="{{ $shop->email }}"
                                                   id="email_from" placeholder="Email">
                                        </div>
                                    </div>
                                    @if($_GET['type'] == 'custom')
                                        <div class="form-group">
                                            <label class="col-sm-2 control-label">Select Day</label>
                                            <div class="col-sm-4">
                                                <select class="form-control" name="meta_date">
                                                    <option value="5">5 Day</option>
                                                    <option value="10">10 Day</option>
                                                    <option value="15">15 Day</option>
                                                    <option value="25">25 Day</option>
                                                </select>
                                            </div>
                                        </div>
                                    @endif
                                </div>
                                <input name="shop_id" type="hidden" value="{{ $shop->id }}">
                                <input name="name_from" type="hidden" value="{{ $shop->shop_owner }}">
                                <input name="type" type="hidden" value="{{$_GET['type']}}">
                                <input name="meta" type="hidden" value="date">
                                <input name="id_member" type="hidden"
                                       value="{{ isset ($_GET['id']) ? $_GET['id'] : ''}}">
                                <input type="hidden" name="_token" id="_token" value="{{ csrf_token() }}">
                                <!-- /.box-body -->
                                <div class="box-footer">
                                    <button type="button" class="btn btn-primary next-step">Continue</button>
                                </div>
                                <!-- /.box-footer -->
                            </div>
                        </div>
                        <div class="tab-pane" role="tabpanel" id="step2">
                            <div class="form-group">
                                <label for="email_template" class="col-sm-2 control-label">Email Template</label>

                                <div class="col-sm-10">
                                    <input type="text" class="form-control" name="email_template" value=""
                                           id="email_template"
                                           placeholder="Email Template">
                                </div>
                            </div>
                            <ul class="list-inline pull-right">
                                <li>
                                    <button type="button" class="btn btn-default prev-step">Previous</button>
                                </li>
                                <li>
                                    <button type="button" class="btn btn-primary next-step">Continue</button>
                                </li>
                            </ul>
                        </div>
                        {{--<div class="tab-pane" role="tabpanel" id="step3">
                            <h3>Step 3</h3>
                            <p>This is step 3</p>
                            <ul class="list-inline pull-right">
                                <li>
                                    <button type="button" class="btn btn-default prev-step">Previous</button>
                                </li>
                                <li>
                                    <button type="button" class="btn btn-default next-step">Skip</button>
                                </li>
                                <li>
                                    <button type="button" class="btn btn-primary btn-info-full next-step">Save and
                                        continue
                                    </button>
                                </li>
                            </ul>
                        </div>--}}
                        <div class="tab-pane" role="tabpanel" id="complete">
                            <h3>Complete</h3>
                            <p>You have successfully completed all steps.</p>
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </form>
            </div>
        </section>
    </div>
@endsection
@section('scripts')
    <script type="text/javascript" src="{{ mix('dist/js/pages/campaigns_auto.min.js') }}"></script>
@endsection
@section('styles')
    <link rel="stylesheet" type="text/css" href="{{ mix('dist/css/campaigns_auto.min.css') }}">
@endsection
