<!DOCTYPE html>
<html>
<head>
    @include('layout.header')
</head>
<body class="hold-transition register-page">
<div class="register-box">
    <div class="register-logo">
        <a href="{{ route('summary.dashboard') }}"><b>Admin</b>LTE</a>
    </div>

    <div class="register-box-body">
        @include('sections.alert_error')
        @include('sections.alert_success')

        <form action="{{ route('users.forgotPasswordHandle') }}" method="post">
            {{ csrf_field() }}
            <div class="form-group has-feedback">
                <input type="email" class="form-control" name="email" value="{{ old('email') }}" placeholder="Email">
                <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
            </div>
            <span class="error error-email">{{ $errors->default->first('email') }}</span>
             <div class="row clearfix">
                <div class="col-xs-12">
                    <button type="submit" class="btn btn-primary btn-block btn-flat">Forgot password</button>
                </div>
                <!-- /.col -->
            </div>
        </form>

        <a href="{{ route('users.login') }}" class="text-center">I already have a membership</a>
    </div>
    <!-- /.form-box -->
</div>
<!-- /.register-box -->

@include('layout.footer')
</body>
</html>
