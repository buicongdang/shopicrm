<!DOCTYPE html>
<html>
<head>
    @include('layout.header')
</head>
<body class="hold-transition skin-blue login-page">
<div class="login-box">
    <div class="login-logo">
        <a href="#"><b>Admin</b>LTE</a>
    </div>
    <!-- /.login-logo -->
    <div class="login-box-body">
        @include('sections.alert_error')
        @include('sections.alert_success')

        <form action="{{ route('users.loginHandle') }}" method="post">
            {{ csrf_field() }}
            <div class="form-group has-feedback">
                <input type="email" value="{{ old('email') }}" name="email" class="form-control" placeholder="Email">
                <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
            </div>
            <span class="error error-email">{{ $errors->default->first('email') }}</span>
            <div class="form-group has-feedback">
                <input type="password" name="password" class="form-control" placeholder="Password">
                <span class="glyphicon glyphicon-lock form-control-feedback"></span>
            </div>
            <span class="error error-email">{{ $errors->default->first('password') }}</span>
            <div class="row">
                <div class="col-xs-8">
                    <div class="checkbox">
                        <label>
                            <input type="checkbox"> Remember Me
                        </label>
                    </div>
                </div>
                <!-- /.col -->
                <div class="col-xs-4">
                    <button type="submit" class="btn btn-primary btn-block btn-flat">Sign In</button>
                </div>
                <!-- /.col -->
            </div>
        </form>

        <a href="{{ route('users.forgotPassword') }}">I forgot my password</a><br>
        <a href="{{ route('users.register') }}" class="text-center">Register a new membership</a>

    </div>
    <!-- /.login-box-body -->
</div>
<!-- /.login-box -->

@include('layout.footer')
</body>
</html>
