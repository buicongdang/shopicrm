@extends('layout.layout_simple')

@section('container_content')
    <div class="clearfix">
        <div class="col-sm-8 col-sm-offset-2">
            <div class="body-logo text-center">
                <a href="#"><h1>Admin LTE</h1></a>
            </div>
            @include('sections.alert_success')
            @include('sections.alert_error')
            <form method="post" action="{{ route('install.importBaseInfoHandle') }}">
                {{ csrf_field() }}
                <input type="hidden" name="id" value="{{ $shop->id }}">
                <input type="hidden" name="myshopify_domain" value="{{ $shop->myshopify_domain }}">
                <input type="hidden" name="name" value="{{ $shop->name }}">
                <input type="hidden" name="domain" value="{{ $shop->domain }}">
                <input type="hidden" name="plan_name" value="{{ $shop->plan_name }}">
                <input type="hidden" name="shop_created_at" value="{{ $shop->created_at }}">
                <input type="hidden" name="province" value="{{ $shop->province }}">
                <input type="hidden" name="country" value="{{ $shop->country }}">
                <input type="hidden" name="currency" value="{{ $shop->currency }}">
                <input type="hidden" name="iana_timezone" value="{{ $shop->iana_timezone }}">

                <div class="form-group {{ $errors->has('shop_owner') ? 'has-error' : '' }}">
                    <label for="name">@lang('install_app.label_name')</label>
                    <input type="text" class="form-control" name="shop_owner" value="{{ old('shop_owner') ? old('shop_owner') : (isset($shop->shop_owner) ? $shop->shop_owner : '') }}" id="shop_owner" placeholder="Shop Owner">
                    <span class="help-block">{{ $errors->first('shop_owner') }}</span>
                </div>

                <div class="form-group {{ $errors->has('email') ? 'has-error' : '' }}">
                    <label for="name">@lang('install_app.label_email')</label>
                    <input type="text" class="form-control" readonly name="email" value="{{ $shop->email }}" id="email" placeholder="Email">
                    <span class="help-block">{{ $errors->first('email') }}</span>
                </div>

                <div class="form-group {{ $errors->has('password') ? 'has-error' : '' }}">
                    <label for="name">@lang('install_app.label_password')</label>
                    <input type="password" class="form-control" name="password" id="password" placeholder="Password">
                    <span class="help-block">{{ $errors->first('password') }}</span>
                </div>
                <div class="form-group text-center">
                    <button class="btn btn-warning btn-lg">Register</button>
                </div>
            </form>
        </div>
    </div>
@endsection