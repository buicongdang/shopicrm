@extends('layout.layout_simple')

@section('container_content')
    <div class="body-logo">
        <a href="#"><h1>Admin LTE</h1></a>
    </div>
    <h1 class="text-center">Add Shop</h1>
    <form method="post" action="{{ route('install.importApiHandle') }}">
        <button>Add Shop to App</button>
    </form>
@endsection