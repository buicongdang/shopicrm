@extends('layout.layout_simple')

@section('container_content')
        <div class="body-logo">
            <a href="#"><h1>Admin LTE</h1></a>
        </div>
        <h1>Import API</h1>
        <form method="post" action="{{ route('install.importApiHandle') }}">
            {{ csrf_field() }}
            <div>Please click button import products, order, customers</div>
            <button>Import API</button>
        </form>
@endsection