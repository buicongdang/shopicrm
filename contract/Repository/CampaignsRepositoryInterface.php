<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 9/1/2017
 * Time: 11:08 AM
 */

namespace Contract\Repository;


interface CampaignsRepositoryInterface
{
    /**
     * @param array $data
     * @return mixed
     */
    public function insert(array $data = []);

    /**
     * @param array $data
     * @return mixed
     */
    public function update(array $data = []);
}