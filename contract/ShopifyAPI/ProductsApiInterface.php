<?php
/**
 * Created by PhpStorm.
 * User: buicongdang
 * Date: 8/19/17
 * Time: 12:14 PM
 */

namespace Contract\ShopifyAPI;


interface ProductsApiInterface
{
    /**
     * @param array $field
     * @param int $limit
     * @param int $page
     * @return mixed
     */
    public function all(array $field, int $limit, int $page) : array ;

    /**
     * @param array $field
     * @param string $product
     * @return array
     */
    public function detail(array $field, string $product): array ;

    /**
     * @param string $status
     * @return array
     */
    public function count(string $status) : array ;

    /**
     * @param array $data
     * @return array
     */
    public function create(array $data) : array ;

    /**
     * @param string $product
     * @param array $data
     * @return array
     */
    public function update(string $product, array $data) : array;

    /**
     * @param string $product
     * @return array
     */
    public function delete(string $product) : array ;
}