<?php

namespace Contract\ShopifyAPI;


interface ShopsApiInterface
{
    /**
     * @param $accessToken
     * @param $shopDomain
     * @return mixed
     */
    public function get();
}