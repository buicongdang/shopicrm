var $ = jQuery.noConflict();
$(document).ready(function () {
    if ($('#featured_image').length)
        el("featured_image").addEventListener("change", readImage, false);

    $('#datepicker').datepicker({
        autoclose: true
    });
    var i = $('#p_emails p').length + 1;
    $('.addScnt').on('click', function(event) {
        var currentlist = $(this).closest('div').attr('id');
        var name = '';
        var style = '';
        if(currentlist === 'p_emails')
        {
            name = 'emails';
            style = '<i class="fa fa-envelope"></i>';
        }
        else
        {
            name = 'phones';
            style = '<i class="fa fa-phone"></i>';
        }
        var scntDiv = $('#'+ currentlist);
        $('<div class="input-group '+ name +'"><div class="input-group-addon">'+ style +'</div><input type="text" class="form-control"  size="20" name="'+ name + '[]" value="" /><span class="input-group-btn"><button type="button"  data-type ="'+ name +'" class="btn btn-info btn-flat remScnt"><i class="fa fa-trash-o"></i></button></span></div>').appendTo(scntDiv);
        i++;
        return false;
    });
    $('body').on('click', '.remScnt', function () {
        var currentlist = $(this).attr('data-type');
        $(this).parents('.'+ currentlist).remove();
    });
});
function el(id) {
    return document.getElementById(id);
} // Get elem by ID
function readImage() {
    if (this.files && this.files[0]) {
        var FR = new FileReader();
        FR.onload = function (e) {
            el("featured_image_result").src = e.target.result;
        };
        FR.readAsDataURL(this.files[0]);
    }
}