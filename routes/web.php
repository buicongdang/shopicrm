<?php

//Summary dashboard
Route::group(['middleware' => 'shopify.check'], function () {
    Route::get('', 'SummaryController@dashboard')->name('summary.dashboard');
});

//Login and register
Route::group([], function () {
    Route::get('login', 'UsersController@login')->name('users.login');
    Route::post('loginHandle', 'UsersController@loginHandle')->name('users.loginHandle');
    Route::get('register', 'UsersController@register')->name('users.register');
    Route::post('registerHandle', 'UsersController@registerHandle')->name('users.registerHandle');
    Route::get('logout', 'UsersController@logout')->name('users.logout');
    Route::get('forgotPassword', 'UsersController@forgotPassword')->name('users.forgotPassword');
    Route::post('forgotPasswordHandle', 'UsersController@forgotPasswordHandle')->name('users.forgotPasswordHandle');
    Route::get('profile', 'UsersController@profile')->name('users.profile');
});


/**
 * Membership router
 *
 **/
Route::group(['prefix' => 'membership', 'middleware' => 'shopify.check'], function () {
    Route::get('', 'MembershipController@all')->name('membership.list');
    Route::get('create', 'MembershipController@create')->name('membership.create');
    Route::post('create', 'MembershipController@createHandle')->name('membership.createHandle');
    Route::get('update/{id}', 'MembershipController@update')->name('membership.update');
    Route::post('update/{id}', 'MembershipController@updateHandle')->name('membership.updateHandle');
    Route::post('ajax-delete', 'MembershipController@ajaxDelete')->name('membership.delete');
});
/**
 *  Customer router
 */
Route::group(['prefix' => 'customers', 'middleware' => 'shopify.check'], function () {
    Route::get('', 'CustomerController@index')->name('customer.list');
    Route::get('created', 'CustomerController@created')->name('customer.created');
    Route::post('createHandle', 'CustomerController@createHandle')->name('customer.createHandle');
    Route::get('update/{id}', 'CustomerController@update')->name('customer.update');
    Route::post('updateHandle', 'CustomerController@updateHandle')->name('customer.updateHandle');
});
/**
 * Import Product
 */
Route::group([], function () {
    Route::get('product', 'ProductsController@importProduct')->name('product.import');
});


/**
 * Import orders
 */
Route::group([], function() {
    Route::get('orders', 'OrdersController@importOrder')->name('orders.import');
});


/**
 * Test route
 */


Route::get('stripe', 'StripeController@index');
Route::post('stripe', 'StripeController@subscription');
Route::post('stripe/webhook', 'StripeHookController@handleWebhook');

/**
 * Shopify route: Install app,...
 */
Route::group([], function (){
    Route::get('install', 'ShopifyController@installApp')->name('shopify.installApp');
    Route::post('installHandle', 'ShopifyController@installAppHandle')->name('shopify.installAppHandle');
    Route::get('authApp', 'ShopifyController@authApp')->name('shopify.authApp');
});
Route::group(['prefix' => 'install', 'middleware' => 'shopify.check'], function(){
    Route::get('importBaseInfo', 'InstallController@importBaseInfo')->name('install.importBaseInfo');
    Route::post('importBaseInfoHandle', 'InstallController@importBaseInfoHandle')->name('install.importBaseInfoHandle');
    Route::get('importApi','InstallController@importApi')->name('install.importApi');
    Route::post('importApiHandle','InstallController@importApiHandle')->name('install.importApiHandle');
});

/**
 * Campaigns Auto
 */
Route::group(['prefix' => 'campaign_auto', 'middleware' => 'shopify.check'], function () {
    Route::get('home', 'CampaignsAutoController@home')->name('campaigns_auto.home');
    Route::get('stepOne', 'CampaignsAutoController@stepOne')->name('campaigns_auto.stepOne');
    Route::post('createHandle', 'CampaignsAutoController@createHandle')->name('campaigns_auto.createHandle');

});

Route::group(['prefix' => 'email_builder', 'middleware' => 'cors'], function () {
   Route::post('saveEmailTemplate', 'EmailBuilderController@saveEmailTemplate');
});

Route::group(['prefix' => 'test'], function() {
    Route::get('channel', function () {
        return view('test.channel');
//        event(new \App\Events\InitAppEvent());
    });
    Route::get('post-channel', function () {
       event(new \App\Events\InitAppEvent());
    });
});